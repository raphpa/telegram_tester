﻿//-----------------------------------------------------------------------
// <copyright file="IOService.cs" company="Raphael Pala">
//     Copyright (c) Raphael Pala. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace Pala.TelegramTester
{
    using NLog;
    using System.Diagnostics.CodeAnalysis;
    using System.IO;
    using System.Windows.Forms;

    /// <summary>
    /// Defines the <see cref="IOService" />
    /// </summary>
    public class IOService : IIOService
    {
        #region Fields

        /// <summary>
        /// Defines the _logger
        /// </summary>
        private static readonly Logger _logger = NLog.LogManager.GetCurrentClassLogger();

        #endregion

        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="IOService"/> class.
        /// </summary>
        public IOService()
        {
            //Try to open last used settings file automatically. Only look for 'settings.xml' in current working directory
            string file = Directory.GetCurrentDirectory() + "\\Settings.xml";
            _logger.Debug("Trying to load {0}. Exists: {1}", file, File.Exists(file));
            if (File.Exists(file))
            {
                SettingsFile = file;
            }
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets the LogFile
        /// </summary>
        public string LogFile { get; private set; }

        /// <summary>
        /// Gets or sets the SettingsFile
        /// </summary>
        public string SettingsFile { get; private set; }

        #endregion

        #region Methods

        /// <summary>
        /// The ChooseLogFileSave
        /// </summary>
        public void ChooseLogFileSave()
        {
            SaveFileDialog saveFileDialog = new SaveFileDialog
            {
                DefaultExt = "csv",
                FileName = "Telegram_Log.csv",
                AddExtension = true,
                OverwritePrompt = true,
                RestoreDirectory = true,
                Filter = "*.csv|*.csv"
            };

            LogFile = (saveFileDialog.ShowDialog() == DialogResult.OK) ? saveFileDialog.FileName : null;
        }

        /// <summary>
        /// The ChooseSettingsFileOpen
        /// </summary>
        public void ChooseSettingsFileOpen()
        {
            OpenFileDialog openFileDialog = new OpenFileDialog
            {
                Filter = "*.xml|*.xml"
            };

            SettingsFile = (openFileDialog.ShowDialog() == DialogResult.OK) ? openFileDialog.FileName : null;
        }

        /// <summary>
        /// The ChooseFileSave
        /// </summary>
        public void ChooseSettingsFileSave()
        {
            SaveFileDialog saveFileDialog = new SaveFileDialog
            {
                DefaultExt = "xml",
                FileName = "Settings.xml",
                AddExtension = true,
                OverwritePrompt = true,
                RestoreDirectory = true,
                Filter = "*.xml|*.xml"
            };

            SettingsFile = (saveFileDialog.ShowDialog() == DialogResult.OK) ? saveFileDialog.FileName : null;
        }

        /// <summary>
        /// The LoadText
        /// </summary>
        /// <returns>The <see cref="string"/></returns>
        [SuppressMessage("Microsoft.Design", "IDE0034")]
        public string LoadSettings()
        {
            if (SettingsFile != null)
            {
                return File.ReadAllText(SettingsFile);
            }
            else
            {
                return default(string);
            }
        }

        /// <summary>
        /// The WriteLog
        /// </summary>
        /// <param name="text">The <see cref="string"/></param>
        public void WriteLog(string text)
        {
            if (LogFile != null)
            {
                File.WriteAllText(LogFile, text);
            }
        }

        /// <summary>
        /// The WriteText
        /// </summary>
        /// <param name="text">The <see cref="string"/></param>
        public void WriteSettings(string text)
        {
            if (SettingsFile != null)
            {
                File.WriteAllText(SettingsFile, text);
            }
        }

        #endregion
    }
}
