﻿//-----------------------------------------------------------------------
// <copyright file="AccentColorData.cs" company="Raphael Pala">
//     Copyright (c) Raphael Pala. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace Pala.TelegramTester
{
    using System.Windows.Media;

    /// <summary>
    /// Defines the <see cref="AccentColorData" />
    /// </summary>
    internal class AccentColorData
    {
        #region Properties

        /// <summary>
        /// Gets or sets the ColorBrush
        /// </summary>
        public Brush ColorBrush { get; set; }

        /// <summary>
        /// Gets or sets the Name
        /// </summary>
        public string Name { get; set; }

        #endregion
    }
}
