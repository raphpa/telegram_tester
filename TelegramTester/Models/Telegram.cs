﻿//-----------------------------------------------------------------------
// <copyright file="Telegram.cs" company="Raphael Pala">
//     Copyright (c) Raphael Pala. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace Pala.TelegramTester
{
    using NLog;
    using System;
    using System.Collections.Generic;
    using System.Globalization;

    /// <summary>
    /// Defines the <see cref="Telegram" />
    /// </summary>
    public class Telegram
    {
        #region Fields

        /// <summary>
        /// Defines the 
        /// </summary>
        private static readonly Logger _logger = NLog.LogManager.GetCurrentClassLogger();

        /// <summary>
        /// Defines the 
        /// </summary>
        private static int _currentTransmissionNumber = 1;

        /// <summary>
        /// Defines the 
        /// </summary>
        private readonly List<object> _dataList = new List<object>();

        /// <summary>
        /// Defines the 
        /// </summary>
        private readonly List<object> _headerList = new List<object>();

        /// <summary>
        /// Defines the 
        /// </summary>
        private string _timeStamp = "";

        #endregion

        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="Telegram"/> class.
        /// </summary>
        /// <param name="direction"></param>
        public Telegram(Direction direction)
        {
            SetTimestampToCurrent();
            Direction = direction;
            _logger.Debug("Created new empty telegram");
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Telegram"/> class.
        /// </summary>
        /// <param name="direction"></param>
        /// <param name="telegramNumber">The <see cref="int"/></param>
        /// <param name="dataList">The <see cref="List{object}"/></param>
        public Telegram(Direction direction, int telegramNumber, List<object> dataList)
        {
            _logger.Debug("Create new telegram with Message ID: {0}", telegramNumber);

            _dataList = dataList;
            TelegramNumber = telegramNumber;
            Direction = direction;
            TransmissionNumber = _currentTransmissionNumber;
            SetTimestampToCurrent();
        }

        #endregion

        #region Properties

        /// <summary>
        /// The GetCurrentTimestamp
        /// </summary>
        /// <returns>The <see cref="string"/></returns>
        public static string CurrentTimestamp
        {
            get
            {
                CultureInfo usCulture = new CultureInfo("en-US");
                DateTime now = DateTime.Now;
                string timestamp = now.ToString("yyyyMMddHHmmss  ", usCulture);

                return timestamp;
            }
        }

        /// <summary>
        /// The GetCurrentTransmissionNumber
        /// </summary>
        /// <returns>The <see cref="int"/></returns>
        public static int CurrentTransmissionNumber
        {
            get
            {
                return _currentTransmissionNumber;
            }
        }

        /// <summary>
        /// The ConvertDataToByteList
        /// </summary>
        /// <returns>The <see cref="List{byte}"/></returns>
        public List<byte> DataAsByteList
        {
            get
            {
                List<byte> byteList = new List<byte>();

                foreach (object obj in _dataList)
                {
                    if (obj is int)
                    {
                        int objInt = TCPService.GetInt((Int32)obj);
                        byte[] byteArray = BitConverter.GetBytes(objInt);
                        foreach (byte b in byteArray)
                        {
                            byteList.Add(b);
                        }
                    }
                    else if (obj is string)
                    {
                        foreach (char c in (string)obj)
                        {
                            byteList.Add(Convert.ToByte(c));
                        }
                    }
                }
                return byteList;
            }
        }

        /// <summary>
        /// Gets the DataAsString
        /// </summary>
        public String DataAsString
        {
            get
            {
                if (Direction == Direction.Receive)
                {
                    return (RawData != null) ? BitConverter.ToString(RawData) : "";
                }
                else
                {
                    return BitConverter.ToString(DataAsByteList.ToArray());
                }
            }
        }

        /// <summary>
        /// Gets or sets the Direction
        /// </summary>
        public Direction Direction { get; set; }

        /// <summary>
        /// Gets or sets the Flags
        /// </summary>
        public short Flags { get; set; } = 0;

        /// <summary>
        /// Gets the FormattedTimestamp
        /// </summary>
        public string FormattedTimestamp
        {
            get
            {
                String _formattedTimestamp = _timeStamp.Substring(6, 2) + "." + _timeStamp.Substring(4, 2) + "." + _timeStamp.Substring(0, 4) + " " +
                                             _timeStamp.Substring(8, 2) + ":" + _timeStamp.Substring(10, 2) + ":" + _timeStamp.Substring(12, 4);
                return _formattedTimestamp;
            }
        }

        /// <summary>
        /// The ConvertHeaderToByteList
        /// </summary>
        /// <returns>The <see cref="List{byte}"/></returns>
        public List<byte> HeaderAsByteList
        {
            get
            {
                List<byte> byteList = new List<byte>();

                List<object> headerList = GetHeaderList();

                foreach (object obj in headerList)
                {
                    if (obj is int)
                    {
                        int objInt = TCPService.GetInt((Int32)obj);
                        byte[] byteArray = BitConverter.GetBytes(objInt);
                        foreach (byte b in byteArray)
                        {
                            byteList.Add(b);
                        }
                    }
                    else if (obj is short)
                    {
                        short objShort = TCPService.GetShort((short)obj);
                        byte[] byteArray = BitConverter.GetBytes(objShort);
                        foreach (byte b in byteArray)
                        {
                            byteList.Add(b);
                        }
                    }
                    else if (obj is ushort)
                    {
                        ushort objShort = TCPService.GetUShort((ushort)obj);
                        byte[] byteArray = BitConverter.GetBytes(objShort);
                        foreach (byte b in byteArray)
                        {
                            byteList.Add(b);
                        }
                    }
                    else if (obj is string)
                    {
                        foreach (char c in (string)obj)
                        {
                            byteList.Add(Convert.ToByte(c));
                        }
                    }
                }

                return byteList;
            }
        }

        /// <summary>
        /// Gets or sets the RawData
        /// </summary>
        public byte[] RawData { get; set; } = null;

        /// <summary>
        /// Gets or sets the ReceivedRawTelegram
        /// </summary>
        public byte[] ReceivedRawTelegram { get; set; } = null;

        /// <summary>
        /// The GetTelegramAsByteArray
        /// </summary>
        /// <returns>The <see cref="byte[]"/></returns>
        public byte[] TelegramAsByteArray
        {
            get
            {
                //Get header bytelist and data bytelist and combine them
                List<byte> telegramByteList = new List<byte>();

                telegramByteList.AddRange(HeaderAsByteList);
                telegramByteList.AddRange(DataAsByteList);

                //Calculate length in bytes, add 4 bytes for length information
                int telegramLength = (telegramByteList.Count + 4);

                _logger.Debug("Creating telegram byte array. Complete length: {0}", telegramLength);

                //Convert to Big Endian Encoding if requested
                telegramLength = TCPService.GetInt(telegramLength);

                //Convert length to byte array
                byte[] lengthByteArray = BitConverter.GetBytes(telegramLength);

                //Convert telegram to byte array
                byte[] telegramByteArray = telegramByteList.ToArray();

                //Generate complete byte array
                byte[] byteArray = new byte[lengthByteArray.Length + telegramByteArray.Length];
                lengthByteArray.CopyTo(byteArray, 0);
                telegramByteArray.CopyTo(byteArray, lengthByteArray.Length);

                return byteArray;
            }
        }

        /// <summary>
        /// Gets or sets the TelegramNumber
        /// </summary>
        public int TelegramNumber { get; set; } = 0;

        /// <summary>
        /// Gets or sets the TimeStamp
        /// </summary>
        public string TimeStamp
        {
            get
            {
                return _timeStamp.PadRight(16);
            }
            set => _timeStamp = value;
        }

        /// <summary>
        /// Gets or sets the TransmissionNumber
        /// </summary>
        public int TransmissionNumber { get; set; } = 0;

        /// <summary>
        /// The GetDataLength
        /// </summary>
        /// <returns>The <see cref="int"/></returns>
        private int DataLength
        {
            get
            {
                List<byte> dataByteList = DataAsByteList;

                return dataByteList.Count;
            }
        }

        #endregion

        #region Methods

        /// <summary>
        /// The SetNextTransmissionNumber
        /// </summary>
        public static void SetNextTransmissionNumber()
        {
            _currentTransmissionNumber++;
            if (_currentTransmissionNumber < 1)
            {
                _currentTransmissionNumber = 1;
            }

            _logger.Debug("Created Transmission Number: {0}", _currentTransmissionNumber);
        }

        /// <summary>
        /// The SetTimestampToCurrent
        /// </summary>
        /// <returns>The <see cref="string"/></returns>
        public string SetTimestampToCurrent()
        {
            CultureInfo usCulture = new CultureInfo("en-US");
            DateTime now = DateTime.Now;
            string timestamp = now.ToString("yyyyMMddHHmmss  ", usCulture);

            _logger.Debug("Created timestamp: {0}", timestamp);

            _timeStamp = timestamp;

            return timestamp;
        }

        /// <summary>
        /// The GetHeaderList
        /// </summary>
        /// <returns>The <see cref="List{object}"/></returns>
        private List<object> GetHeaderList()
        {
            List<object> headerList = new List<object>
            {
                /*
                 * Header: Message ID(int), Transmission Number(int), Timestamp(string)
                 */
                TelegramNumber,
                _currentTransmissionNumber,
                TimeStamp
            };

            return headerList;
        }

        #endregion
    }
}
