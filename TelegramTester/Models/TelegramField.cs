﻿//-----------------------------------------------------------------------
// <copyright file="TelegramField.cs" company="Raphael Pala">
//     Copyright (c) Raphael Pala. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace Pala.TelegramTester
{
    using NLog;
    using System;
    using System.Collections;
    using System.ComponentModel;
    using System.Runtime.CompilerServices;

    /// <summary>
    /// Defines the <see cref="TelegramField" />
    /// </summary>
    public class TelegramField : INotifyPropertyChanged
    {
        #region Fields

        /// <summary>
        /// Defines the _logger = NLog.LogManager.GetCurrentClassLogger()
        /// </summary>
        private static readonly Logger _logger = NLog.LogManager.GetCurrentClassLogger();

        /// <summary>
        /// Defines the _datatype
        /// </summary>
        private Datatype _datatype = Datatype.DWord;

        /// <summary>
        /// Defines the _fieldDescription
        /// </summary>
        private string _fieldDescription;

        /// <summary>
        /// Defines the _valueDataChars
        /// </summary>
        private string _valueDataChars;

        /// <summary>
        /// Defines the _valueDataCharsCount
        /// </summary>
        private int _valueDataCharsCount = 1;

        /// <summary>
        /// Defines the _valueDataDWord
        /// </summary>
        private int _valueDataDWord = 0;

        #endregion

        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="TelegramField"/> class.
        /// </summary>
        public TelegramField()
        {
            GUID = Guid.NewGuid();
            _logger.Debug("Created new telegram field with guid: {0}", GUID);
        }

        #endregion

        #region Events

        /// <summary>
        /// Defines the PropertyChanged
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;

        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets the Datatype
        /// </summary>
        public Datatype Datatype
        {
            get => _datatype;
            set
            {
                _logger.Debug("Changing datatype to {0}", value);
                _datatype = value;
                NotifyPropertyChanged(nameof(Datatype));
            }
        }

        /// <summary>
        /// Gets or sets the FieldDescription
        /// </summary>
        public string FieldDescription
        {
            set
            {
                _fieldDescription = value;
                NotifyPropertyChanged(nameof(FieldDescription));
            }

            get
            {
                //if field is repeat group start or repeat group end, override field description
                if (Datatype == Datatype.RepeatGroupStart)
                {
                    return "---- " + ValueDataDWord.ToString() + "x ----";
                }
                else if (Datatype == Datatype.RepeatGroupEnd)
                {
                    return "------------";
                }
                else
                {
                    return _fieldDescription;
                }
            }
        }

        /// <summary>
        /// Gets the GUID
        /// </summary>
        public Guid GUID { get; }

        /// <summary>
        /// Gets or sets the ValueDataBitfield
        /// </summary>
        public bool[] ValueDataBitfield { get; private set; }

        /// <summary>
        /// Gets or sets the ValueDataChars
        /// </summary>
        public String ValueDataChars
        {
            get => _valueDataChars;
            set
            {
                _logger.Debug("Setting chars to: {0}", value);
                _valueDataChars = value;
                NotifyPropertyChanged(nameof(ValueDataChars));
            }
        }

        /// <summary>
        /// Gets or sets the ValueDataCharsCount
        /// </summary>
        public int ValueDataCharsCount
        {
            get => _valueDataCharsCount;
            set
            {
                _logger.Debug("Setting char count to {0}", value);
                _valueDataCharsCount = value;
                NotifyPropertyChanged(nameof(ValueDataCharsCount));

                //Remove chars if current string length is more than set char count
                if (value > 0 && ValueDataChars.Length > value)
                {
                    ValueDataChars = ValueDataChars.Substring(0, value);
                }
            }
        }

        /// <summary>
        /// Gets or sets the ValueDataDWord
        /// </summary>
        public int ValueDataDWord
        {
            get => _valueDataDWord;
            set
            {
                _valueDataDWord = value;
                _logger.Debug("Set DWord to: {0}", value);

                BitArray bitArray = new BitArray(new[] { value });
                bool[] boolArray = new bool[32];
                bitArray.CopyTo(boolArray, 0);

                ValueDataBitfield = boolArray;

                if (Datatype == Datatype.RepeatGroupStart || Datatype == Datatype.RepeatGroupEnd)
                {
                    NotifyPropertyChanged("FieldDescription");
                }
            }
        }

        #endregion

        #region Methods

        /// <summary>
        /// The NotifyPropertyChanged
        /// </summary>
        /// <param name="propertyName">The <see cref="String"/></param>
        private void NotifyPropertyChanged([CallerMemberName] String propertyName = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        #endregion
    }
}
