﻿//-----------------------------------------------------------------------
// <copyright file="TelegramDefinition.cs" company="Raphael Pala">
//     Copyright (c) Raphael Pala. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace Pala.TelegramTester
{
    using NLog;
    using System;
    using System.ComponentModel;
    using System.Linq;
    using System.Windows.Data;

    /// <summary>
    /// Defines the <see cref="TelegramDefinition" />
    /// </summary>
    public class TelegramDefinition : IComparable
    {
        #region Fields

        /// <summary>
        /// Defines the _logger = NLog.LogManager.GetCurrentClassLogger()
        /// </summary>
        private static readonly Logger _logger = NLog.LogManager.GetCurrentClassLogger();

        /// <summary>
        /// Defines the _fieldListLock = new object()
        /// </summary>
        private readonly object _fieldListLock = new object();

        #endregion

        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="TelegramDefinition"/> class.
        /// </summary>
        public TelegramDefinition()
        {
            FieldList = new BindingList<TelegramField>();
            BindingOperations.EnableCollectionSynchronization(FieldList, _fieldListLock);

            GUID = Guid.NewGuid();
            _logger.Debug("Created new telegram definition with GUID: {0}", GUID);
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets the Direction
        /// </summary>
        public Direction Direction { get; set; }

        /// <summary>
        /// Gets the FieldList
        /// </summary>
        public BindingList<TelegramField> FieldList { get; }

        /// <summary>
        /// Gets or sets a value indicating whether IsAckTelegram
        /// </summary>
        public bool IsAckTelegram { get; set; }

        /// <summary>
        /// Gets or sets the TelegramDescription
        /// </summary>
        public string TelegramDescription { get; set; }

        /// <summary>
        /// Gets or sets the Message ID
        /// </summary>
        public int TelegramNumber { get; set; }

        /// <summary>
        /// Gets the GUID
        /// </summary>
        private Guid GUID { get; }

        #endregion

        #region Methods

        /// <summary>
        /// The AddField
        /// </summary>
        /// <param name="fieldDescription">The <see cref="string"/></param>
        /// <param name="datatype">The <see cref="Datatype"/></param>
        /// <param name="value">The <see cref="object"/></param>
        public TelegramField AddField(string fieldDescription)
        {
            _logger.Debug("Trying to add telegram field with description: {0}", fieldDescription);
            TelegramField telegramField = new TelegramField
            {
                FieldDescription = fieldDescription,
                Datatype = Datatype.DWord,
                ValueDataChars = "",
                ValueDataDWord = 0
            };

            FieldList.Add(telegramField);

            _logger.Debug("Added field {0} in telegram {1}", fieldDescription, TelegramNumber);

            return telegramField;
        }

        /// <summary>
        /// The AddRepeatGroup
        /// </summary>
        /// <returns>The <see cref="TelegramField"/></returns>
        public TelegramField AddRepeatGroup()
        {
            _logger.Debug("Trying to add repeat group");
            TelegramField repeatGroupStart = new TelegramField
            {
                FieldDescription = "",
                Datatype = Datatype.RepeatGroupStart,
                ValueDataChars = "",
                ValueDataDWord = 1
            };
            FieldList.Add(repeatGroupStart);

            TelegramField repeatGroupEnd = new TelegramField
            {
                FieldDescription = "",
                Datatype = Datatype.RepeatGroupEnd,
                ValueDataChars = "",
                ValueDataDWord = 0
            };
            FieldList.Add(repeatGroupEnd);

            _logger.Debug("Added repeat group in telegram {0}", TelegramNumber);

            return repeatGroupStart;
        }

        /// <summary>
        /// The ClearFieldList
        /// </summary>
        public void ClearFieldList()
        {
            _logger.Debug("Clearing telegram fields");
            FieldList.Clear();
        }

        /// <summary>
        /// The CompareTo
        /// </summary>
        /// <param name="obj">The <see cref="object"/></param>
        /// <returns>The <see cref="int"/></returns>
        public int CompareTo(object obj)
        {
            if (obj is TelegramDefinition)
            {
                return TelegramNumber.CompareTo((obj as TelegramDefinition)?.TelegramNumber);
            }
            throw new ArgumentException("Object is not TelegramDefinition");
        }

        /// <summary>
        /// The DeleteField
        /// </summary>
        /// <param name="telegramField">The <see cref="TelegramField"/></param>
        public void DeleteField(TelegramField telegramField)
        {
            if (telegramField.Datatype == Datatype.RepeatGroupStart)
            {
                _logger.Debug("Trying to delete repeat group (Starting at Start)");
                //If requested field to delete is a repeat group start, also delete following repeat group end

                //Get field position of repeat group start
                int fieldPosition = GetFieldPosition(telegramField);

                //Walk forward through field list until repeat group end is found
                for (int i = fieldPosition; i < FieldList.Count; i++)
                {
                    //If repeat group end was found, delete and exit loop
                    if (FieldList[i].Datatype == Datatype.RepeatGroupEnd)
                    {
                        FieldList.Remove(FieldList[i]);
                        break;
                    }
                }

                //Remove repeat group start
                FieldList.Remove(telegramField);
            }
            else if (telegramField.Datatype == Datatype.RepeatGroupEnd)
            {
                _logger.Debug("Trying to delete repeat group (Starting at End)");
                //If requested field to delete is a repeat group end, also delete previous repeat group start

                //Get field position of repeat group end
                int fieldPosition = GetFieldPosition(telegramField);

                //Walk backward through field list until repeat group start is found
                for (int i = fieldPosition; i >= 0; i--)
                {
                    //If repeat group start was found, delete and exit loop
                    if (FieldList[i].Datatype == Datatype.RepeatGroupStart)
                    {
                        FieldList.Remove(FieldList[i]);
                        break;
                    }
                }

                //Remove repeat group end
                FieldList.Remove(telegramField);
            }
            else
            {
                _logger.Debug("Trying to delete telegram field");
                FieldList.Remove(telegramField);
            }
        }

        /// <summary>
        /// The GetFieldBytePosition
        /// </summary>
        /// <param name="telegramField">The <see cref="TelegramField"/></param>
        /// <returns>The <see cref="int"/></returns>
        public int GetFieldBytePosition(TelegramField telegramField)
        {
            int byteCount = 0;

            //Run through all fields in field list and add up byte count until given field is found
            foreach (TelegramField field in FieldList)
            {
                if (field.GUID == telegramField.GUID)
                {
                    break;
                }

                byteCount += GetFieldLength(field);
            }

            //Add offset for telegram header
            int headerOffset = new Telegram(Direction).TelegramAsByteArray.Length;

            byteCount += headerOffset;

            return byteCount;
        }

        /// <summary>
        /// The GetFieldLength
        /// </summary>
        /// <param name="telegramField">The <see cref="TelegramField"/></param>
        /// <returns>The <see cref="int"/></returns>
        public int GetFieldLength(TelegramField telegramField)
        {
            if (telegramField.Datatype == Datatype.DWord)
            {
                return 4;
            }
            else if (telegramField.Datatype == Datatype.Bitfield)
            {
                return 4;
            }
            else if (telegramField.Datatype == Datatype.Chars)
            {
                return telegramField.ValueDataCharsCount;
            }
            else
            {
                return 0;
            }
        }

        /// <summary>
        /// The GetFieldPosition
        /// </summary>
        /// <param name="telegramField">The <see cref="TelegramField"/></param>
        /// <returns>The <see cref="int"/></returns>
        public int GetFieldPosition(TelegramField telegramField)
        {
            return FieldList.IndexOf(FieldList.FirstOrDefault(item => item.GUID == telegramField.GUID));
        }

        /// <summary>
        /// The MoveFieldDown
        /// </summary>
        /// <param name="telegramField">The <see cref="TelegramField"/></param>
        public void MoveFieldDown(TelegramField telegramField)
        {
            _logger.Debug("Trying to move telegram field down.");

            //Get current position of field
            int currentIndex = GetFieldPosition(telegramField);
            _logger.Debug("Current index found: {0}", currentIndex);

            //if position is not at the end of list, move field down
            if (currentIndex >= 0 && currentIndex < FieldList.Count - 1)
            {
                //if field is repeat group start and next field is repeat group end, do not move (start is not 
                //allowed to move below end)
                if (telegramField.Datatype == Datatype.RepeatGroupStart && FieldList[currentIndex + 1].Datatype == Datatype.RepeatGroupEnd)
                {
                    return;
                }
                //if field is repeat group end and next field is repeat group start, do not move (end is not 
                //allowed to move below start)
                else if (telegramField.Datatype == Datatype.RepeatGroupEnd && FieldList[currentIndex + 1].Datatype == Datatype.RepeatGroupStart)
                {
                    return;
                }
                else
                {
                    //Move field
                    TelegramField field = FieldList[currentIndex];
                    FieldList.RemoveAt(currentIndex);
                    FieldList.Insert(++currentIndex, field);
                }
            }
        }

        /// <summary>
        /// The MoveFieldUp
        /// </summary>
        /// <param name="telegramField">The <see cref="TelegramField"/></param>
        public void MoveFieldUp(TelegramField telegramField)
        {
            _logger.Debug("Trying to move telegram field up.");

            //Get current position of field
            int currentIndex = GetFieldPosition(telegramField);
            _logger.Debug("Current index found: {0}", currentIndex);

            //if position is not at the start of list, move field up
            if (currentIndex > 0)
            {
                //if field is repeat group start and previous field is repeat group end, do not move (start is not 
                //allowed to move above end)
                if (telegramField.Datatype == Datatype.RepeatGroupStart && FieldList[currentIndex - 1].Datatype == Datatype.RepeatGroupEnd)
                {
                    return;
                }
                //if field is repeat group end and next field is repeat group start, do not move (end is not 
                //allowed to move above start)
                else if (telegramField.Datatype == Datatype.RepeatGroupEnd && FieldList[currentIndex - 1].Datatype == Datatype.RepeatGroupStart)
                {
                    return;
                }
                else
                {
                    //Move field up
                    TelegramField field = FieldList[currentIndex];
                    FieldList.RemoveAt(currentIndex);
                    FieldList.Insert(--currentIndex, field);
                }
            }
        }

        #endregion
    }
}
