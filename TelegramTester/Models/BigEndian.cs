﻿//-----------------------------------------------------------------------
// <copyright file="BigEndian.cs" company="Raphael Pala">
//     Copyright (c) Raphael Pala. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace Pala.TelegramTester
{
    /// <summary>
    /// Defines the <see cref="BigEndian" />
    /// </summary>
    public static class BigEndian
    {
        #region Methods

        /// <summary>
        /// The FromBigEndian
        /// </summary>
        /// <param name="value">The <see cref="int"/></param>
        /// <returns>The <see cref="int"/></returns>
        public static int FromBigEndian(this int value)
        {
            return System.Net.IPAddress.NetworkToHostOrder(value);
        }

        /// <summary>
        /// The FromBigEndian
        /// </summary>
        /// <param name="value">The <see cref="long"/></param>
        /// <returns>The <see cref="long"/></returns>
        public static long FromBigEndian(this long value)
        {
            return System.Net.IPAddress.NetworkToHostOrder(value);
        }

        /// <summary>
        /// The FromBigEndian
        /// </summary>
        /// <param name="value">The <see cref="short"/></param>
        /// <returns>The <see cref="short"/></returns>
        public static short FromBigEndian(this short value)
        {
            return System.Net.IPAddress.NetworkToHostOrder(value);
        }

        /// <summary>
        /// The ToBigEndian
        /// </summary>
        /// <param name="value">The <see cref="int"/></param>
        /// <returns>The <see cref="int"/></returns>
        public static int ToBigEndian(this int value)
        {
            return System.Net.IPAddress.HostToNetworkOrder(value);
        }

        /// <summary>
        /// The ToBigEndian
        /// </summary>
        /// <param name="value">The <see cref="long"/></param>
        /// <returns>The <see cref="long"/></returns>
        public static long ToBigEndian(this long value)
        {
            return System.Net.IPAddress.HostToNetworkOrder(value);
        }

        /// <summary>
        /// The ToBigEndian
        /// </summary>
        /// <param name="value">The <see cref="short"/></param>
        /// <returns>The <see cref="short"/></returns>
        public static short ToBigEndian(this short value)
        {
            return System.Net.IPAddress.HostToNetworkOrder(value);
        }

        /// <summary>
        /// The ToBigEndian
        /// </summary>
        /// <param name="value">The <see cref="ushort"/></param>
        /// <returns>The <see cref="ushort"/></returns>
        public static ushort ToBigEndian(this ushort value)
        {
            return (ushort)System.Net.IPAddress.HostToNetworkOrder((short)value);
        }

        #endregion
    }
}
