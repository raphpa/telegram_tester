﻿//-----------------------------------------------------------------------
// <copyright file="GenerateTelegrams.cs" company="Raphael Pala">
//     Copyright (c) Raphael Pala. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace Pala.TelegramTester
{
    using NLog;
    using System;
    using System.Collections.Generic;
    using System.Globalization;

    /// <summary>
    /// Defines the <see cref="GenerateTelegrams" />
    /// </summary>
    internal class GenerateTelegrams
    {
        #region Constants

        /// <summary>
        /// Defines the _flags
        /// </summary>
        private const short _flags = 0;

        #endregion

        #region Fields

        /// <summary>
        /// Defines the 
        /// </summary>
        private static readonly Logger _logger = NLog.LogManager.GetCurrentClassLogger();

        /// <summary>
        /// Defines the 
        /// </summary>
        private static int _transmissionNumber = 0;

        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets a value indicating whether UseBigEndian
        /// </summary>
        public bool UseBigEndian { get; set; } = true;

        #endregion

        #region Methods

        /// <summary>
        /// The GenerateTelegram
        /// </summary>
        /// <param name="telegramNumber">The <see cref="int"/></param>
        /// <param name="data">The <see cref="List{object}"/></param>
        /// <returns>The <see cref="byte[]"/></returns>
        public byte[] GenerateTelegram(int telegramNumber, List<object> data)
        {
            List<byte> dataByteList = GenerateData(data);
            List<byte> headerByteList = GenerateHeader(telegramNumber, dataByteList.Count);

            //Generate complete telegram
            List<byte> telegramByteList = new List<byte>();
            telegramByteList.AddRange(headerByteList);
            telegramByteList.AddRange(dataByteList);

            //convert to byte array
            byte[] telegramByteArray = telegramByteList.ToArray();

            return telegramByteArray;
        }

        /// <summary>
        /// The GenerateData
        /// </summary>
        /// <param name="list">The <see cref="List{object}"/></param>
        /// <returns>The <see cref="List{byte}"/></returns>
        private List<byte> GenerateData(List<object> list)
        {
            List<byte> byteList = new List<byte>();

            foreach (object obj in list)
            {
                if (obj is int)
                {
                    int objInt = TCPService.GetInt((Int32)obj);
                    byte[] byteArray = BitConverter.GetBytes(objInt);
                    foreach (byte b in byteArray)
                    {
                        byteList.Add(b);
                    }
                }
                else if (obj is string)
                {
                    foreach (char c in (string)obj)
                    {
                        byteList.Add(Convert.ToByte(c));
                    }
                }
            }

            return byteList;
        }

        /// <summary>
        /// The GenerateHeader
        /// </summary>
        /// <param name="telegramNumber">The <see cref="int"/></param>
        /// <param name="dataLength">The <see cref="int"/></param>
        /// <returns>The <see cref="List{byte}"/></returns>
        private List<byte> GenerateHeader(int telegramNumber, int dataLength)
        {
            _logger.Debug("Creating telegram header. ID: {0}, Data Length: {1}", telegramNumber, dataLength);

            //Create byte list
            List<byte> byteList = new List<byte>();

            //Telegram Length

            //Add given data length to length of header (22 Bytes)
            //Convert to BigEndian if necessary
            int length = (22 + dataLength);
            length = TCPService.GetInt(length);

            //Convert integer into byte array
            byte[] byteArray = BitConverter.GetBytes(length);

            //Put single bytes into the byte list
            foreach (byte b in byteArray)
            {
                byteList.Add(b);
            }

            //Telegram Number

            //Convert to BigEndian if necessary
            int telNumber = TCPService.GetInt(telegramNumber);

            //Convert integer into byte array
            byteArray = BitConverter.GetBytes(telNumber);

            //Put single bytes into the byte list
            foreach (byte b in byteArray)
            {
                byteList.Add(b);
            }

            //Transmission Number

            //Convert to BigEndian if necessary
            int transmissionNumber = TCPService.GetInt(GetTransmissionNumber());

            //Convert integer into byte array
            byteArray = BitConverter.GetBytes(transmissionNumber);

            //Put single bytes into the byte list
            foreach (byte b in byteArray)
            {
                byteList.Add(b);
            }

            //Timestamp

            //Get timestamp as string
            string timestamp = GetTimestamp();

            //Add single characters to byte list
            foreach (char c in timestamp)
            {
                byte b = Convert.ToByte(c);
                byteList.Add(b);
            }

            //return header as List of bytes
            return byteList;
        }

        /// <summary>
        /// The GetTimestamp
        /// </summary>
        /// <returns>The <see cref="string"/></returns>
        private string GetTimestamp()
        {
            CultureInfo usCulture = new CultureInfo("en-US");
            DateTime now = DateTime.Now;
            string timestamp = now.ToString("yyyyMMddHHmmss  ", usCulture);

            _logger.Debug("Created timestamp: {0}", timestamp);

            return timestamp;
        }

        /// <summary>
        /// The GetTransmissionNumber
        /// </summary>
        /// <returns>The <see cref="int"/></returns>
        private int GetTransmissionNumber()
        {
            _transmissionNumber++;
            if (_transmissionNumber > int.MaxValue || _transmissionNumber < 1)
            {
                _transmissionNumber = 1;
            }

            _logger.Debug("Created transmission number: {0}", _transmissionNumber);

            return _transmissionNumber;
        }

        #endregion
    }
}
