﻿//-----------------------------------------------------------------------
// <copyright file="SettingsService.cs" company="Raphael Pala">
//     Copyright (c) Raphael Pala. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace Pala.TelegramTester
{
    using Caliburn.Micro;
    using MahApps.Metro;
    using NLog;
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Collections.Specialized;
    using System.ComponentModel;
    using System.Globalization;
    using System.IO;
    using System.Linq;
    using System.Windows;
    using System.Windows.Data;
    using System.Xml.Linq;

    /// <summary>
    /// Defines the <see cref="SettingsService" />
    /// </summary>
    public class SettingsService : ISettingsService
    {
        #region Constants

        /// <summary>
        /// Defines the version = 1
        /// </summary>
        private const int version = 1;

        #endregion

        #region Fields

        /// <summary>
        /// Defines the 
        /// </summary>
        private static readonly Logger _logger = NLog.LogManager.GetCurrentClassLogger();

        /// <summary>
        /// Defines the _eventAggregator
        /// </summary>
        private readonly IEventAggregator _eventAggregator;

        /// <summary>
        /// Defines the _ioService
        /// </summary>
        private readonly IIOService _ioService;

        /// <summary>
        /// Defines the 
        /// </summary>
        private readonly object _telegramDefinitionsLock = new object();

        /// <summary>
        /// Defines the _telegramDefinitionsSendLock = new object()
        /// </summary>
        private readonly object _telegramDefinitionsSendLock = new object();

        /// <summary>
        /// Defines the _activeConnection
        /// </summary>
        private bool _activeConnection = false;

        /// <summary>
        /// Defines the 
        /// </summary>
        private string _applicationColor = "Cobalt";

        /// <summary>
        /// Defines the 
        /// </summary>
        private Style _applicationStyle = Style.BaseLight;

        /// <summary>
        /// Defines the _autosendAck = true
        /// </summary>
        private bool _autosendAck = true;

        /// <summary>
        /// Defines the 
        /// </summary>
        private bool _bigEndianEncoding = true;

        /// <summary>
        /// Defines the 
        /// </summary>
        private string _language = "en";

        /// <summary>
        /// Defines the _maximumLogCount
        /// </summary>
        private int _maximumLogCount = 500;

        /// <summary>
        /// Defines the _port = 20000
        /// </summary>
        private int _port = 20000;

        /// <summary>
        /// Defines the _remoteIPAddress
        /// </summary>
        private string _remoteIPAddress = "192.168.0.1";

        /// <summary>
        /// Defines the _simulation
        /// </summary>
        private Simulation _simulation = Simulation.Level2;

        #endregion

        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="SettingsService"/> class.
        /// </summary>
        /// <param name="eventAggregator"></param>
        public SettingsService(IEventAggregator eventAggregator)
        {
            _eventAggregator = eventAggregator;
            _eventAggregator.Subscribe(this);

            TelegramDefinitions = new BindingList<TelegramDefinition>();
            TelegramDefinitionsSend = new ObservableCollection<TelegramDefinition>();
            //TelegramDefinitions.CollectionChanged += TelegramDefinitionsChanged;

            BindingOperations.EnableCollectionSynchronization(TelegramDefinitions, _telegramDefinitionsLock);
            BindingOperations.EnableCollectionSynchronization(TelegramDefinitionsSend, _telegramDefinitionsSendLock);
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="SettingsService"/> class.
        /// </summary>
        /// <param name="eventAggregator"></param>
        /// <param name="ioService"></param>
        public SettingsService(IEventAggregator eventAggregator, IIOService ioService) : this(eventAggregator)
        {
            _eventAggregator = eventAggregator;
            _eventAggregator.Subscribe(this);

            _ioService = ioService;

            ChangeStyle();
            SetLanguage();

            //Try to open last used settings file automatically.
            string xml = _ioService.LoadSettings();
            ParseSettings(xml);
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets a value indicating whether ActiveConnection
        /// </summary>
        public bool ActiveConnection
        {
            get => _activeConnection;
            set
            {
                _activeConnection = value;
                _logger.Debug("Setting active connection to: {0}", value);
            }
        }

        /// <summary>
        /// Gets or sets the ApplicationColor
        /// </summary>
        public string ApplicationColor
        {
            get => _applicationColor;
            set
            {
                _applicationColor = value;
                _logger.Debug("Setting application color to {0}", value);

                ChangeStyle();
            }
        }

        /// <summary>
        /// Gets or sets the ApplicationStyle
        /// </summary>
        public Style ApplicationStyle
        {
            get => _applicationStyle;
            set
            {
                _applicationStyle = value;
                _logger.Debug("Setting application style to {0}", value);

                ChangeStyle();
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether AutosendAck
        /// </summary>
        public bool AutosendAck
        {
            get => _autosendAck;
            set
            {
                _autosendAck = value;
                _logger.Debug("Setting Autosend Ack to: {0}", value);
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether BigEndianEncoding
        /// </summary>
        public bool BigEndianEncoding
        {
            get => _bigEndianEncoding;
            set
            {
                _bigEndianEncoding = value;
                _logger.Debug("Setting Big Endian Encoding to: {0}", value);
            }
        }

        /// <summary>
        /// Gets or sets the Language
        /// </summary>
        public string Language
        {
            get => _language;
            set
            {
                _language = value;
                _logger.Debug("Setting language to: {0}", value);

                SetLanguage();
            }
        }

        /// <summary>
        /// Gets or sets the MaximumLogCount
        /// </summary>
        public int MaximumLogCount
        {
            get => _maximumLogCount;
            set
            {
                _maximumLogCount = value;
                _logger.Debug("Setting maximum log count to {0}", value);
            }
        }

        /// <summary>
        /// Gets or sets the Port
        /// </summary>
        public int Port
        {
            get => _port;
            set
            {
                _port = value;
                _logger.Debug("Setting port to: {0}", value);
            }
        }

        /// <summary>
        /// Gets or sets the RemoteHost
        /// </summary>
        public string RemoteHost
        {
            get => _remoteIPAddress;
            set
            {
                _remoteIPAddress = value;
                _logger.Debug("Setting remote IP address to : {0}", value);
            }
        }

        /// <summary>
        /// Gets or sets the Simulation
        /// </summary>
        public Simulation Simulation
        {
            get => _simulation;
            set
            {
                _simulation = value;
                _logger.Debug("Setting simulation to {0}", value);
                TelegramDefinitionsChanged(this, null);
            }
        }

        /// <summary>
        /// Gets the TelegramDefinitions
        /// </summary>
        public BindingList<TelegramDefinition> TelegramDefinitions { get; }

        /// <summary>
        /// Gets the TelegramDefinitionsSend
        /// </summary>
        public ObservableCollection<TelegramDefinition> TelegramDefinitionsSend { get; }

        #endregion

        #region Methods

        /// <summary>
        /// The AddTelegramDefinition
        /// </summary>
        /// <param name="telegramNumber">The <see cref="int"/></param>
        /// <param name="telegramDescription">The <see cref="string"/></param>
        /// <param name="direction">The <see cref="Direction"/></param>
        public TelegramDefinition AddTelegramDefinition(int telegramNumber)
        {
            _logger.Debug("Trying to add telegram defintition with message ID: {0}", telegramNumber);
            //Add telegram only if it is not yet in the list of defined telegrams
            if (TelegramDefinitions.FirstOrDefault(item => item.TelegramNumber == telegramNumber) == null)
            {
                _logger.Debug("Did not find a telegram definition with this number. Adding new telegram.");
                TelegramDefinition telegramDefinition = new TelegramDefinition
                {
                    TelegramNumber = telegramNumber,
                    Direction = Direction.ToPLC,
                    TelegramDescription = ""
                };

                TelegramDefinitions.Add(telegramDefinition);

                SortTelegramDefinitions();

                return telegramDefinition;
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// The ClearTelegramDefinitions
        /// </summary>
        public void ClearTelegramDefinitions()
        {
            TelegramDefinitions.Clear();
        }

        /// <summary>
        /// The DeleteTelegramDefinition
        /// </summary>
        /// <param name="telegramDefinition">The <see cref="TelegramDefinition"/></param>
        public void DeleteTelegramDefinition(TelegramDefinition telegramDefinition)
        {
            _logger.Debug("Trying to delete telegram with message ID: {0}", telegramDefinition.TelegramNumber);
            TelegramDefinition definition = TelegramDefinitions.FirstOrDefault(item => item.TelegramNumber == telegramDefinition.TelegramNumber);
            if (definition != null)
            {
                _logger.Debug("Found telegram definition at index {0}, deleting...", TelegramDefinitions.IndexOf(definition));
                TelegramDefinitions.Remove(definition);

                SortTelegramDefinitions();
            }
        }

        /// <summary>
        /// The FindTelegramDefinition
        /// </summary>
        /// <param name="telegramNumber">The <see cref="int"/></param>
        /// <returns>The <see cref="TelegramDefinition"/></returns>
        public TelegramDefinition FindTelegramDefinition(int telegramNumber)
        {
            _logger.Debug("Searching for telegram {0}", telegramNumber);

            return TelegramDefinitions.FirstOrDefault(item => item.TelegramNumber == telegramNumber);
        }

        /// <summary>
        /// The GetAckTelegram
        /// </summary>
        /// <param name="direction">The <see cref="Direction"/></param>
        /// <returns>The <see cref="int"/></returns>
        public int GetAckTelegram(Direction direction)
        {
            _logger.Debug("Searching for ack telegram for direction {0}", direction);

            TelegramDefinition definition = TelegramDefinitions.FirstOrDefault(item => (item.Direction == direction) && (item.IsAckTelegram));
            _logger.Debug("Found telegram: {0}", definition?.TelegramNumber);
            return definition?.TelegramNumber ?? 0;
        }

        /// <summary>
        /// The LoadSettings
        /// </summary>
        public void LoadSettings()
        {
            _ioService.ChooseSettingsFileOpen();
            _logger.Debug("Deserializing settings from {0}", _ioService.SettingsFile);

            string xml = _ioService.LoadSettings();

            ParseSettings(xml);

            _eventAggregator.PublishOnUIThread(Settings.Reload);
        }

        /// <summary>
        /// The ParseSettings
        /// </summary>
        /// <param name="xml">The <see cref="string"/></param>
        public void ParseSettings(string xml)
        {
            try
            {
                XDocument xdocument = XDocument.Parse(xml);

                if (xdocument.Root.Attribute("Version")?.Value != "1") return;

                ApplicationColor = xdocument.Root.Element("ApplicationColor")?.Value ?? "Cobalt";

                ApplicationStyle = Enum.TryParse(xdocument.Root.Element("ApplicationStyle")?.Value, out Style applicationStyle) ? applicationStyle : Style.BaseLight;

                AutosendAck = xdocument.Root.Element("AutosendAck")?.Value == "true";

                BigEndianEncoding = xdocument.Root.Element("BigEndianEncoding")?.Value == "true";

                Simulation = Enum.TryParse(xdocument.Root.Element("Simulation")?.Value, out Simulation simulation) ? simulation : Simulation.Level2;

                Language = xdocument.Root.Element("Language")?.Value ?? "en";

                ActiveConnection = xdocument.Root.Element("ActiveConnection")?.Value == "true";

                RemoteHost = xdocument.Root.Element("RemoteHost")?.Value ?? "192.168.0.1";

                Port = Int32.TryParse(xdocument.Root.Element("Port")?.Value, out int port) ? port : 20000;

                MaximumLogCount = Int32.TryParse(xdocument.Root.Element("MaximumLogCount")?.Value, out int maximumLogCount) ? maximumLogCount : 500;

                IEnumerable<XElement> telegramDefinitions = xdocument.Root.Element("TelegramDefinitions").Elements("Telegram");

                TelegramDefinitions.Clear();
                foreach (XElement element in telegramDefinitions)
                {
                    TelegramDefinition telegramDefinition = new TelegramDefinition
                    {
                        Direction = Enum.TryParse(element.Element("Direction")?.Value, out Direction direction) ? direction : Direction.ToPLC,
                        TelegramDescription = element.Element("TelegramDescription")?.Value
                    };

                    // Check if ACK telegram exists already for direction. If so, do not add it again
                    if (GetAckTelegram(telegramDefinition.Direction) == 0)
                    {
                        telegramDefinition.IsAckTelegram = element.Element("IsAckTelegram")?.Value == "true";
                    }
                    else
                    {
                        telegramDefinition.IsAckTelegram = false;
                    }

                    if (Int32.TryParse(element.Element("TelegramNumber")?.Value, out int telegramNumber))
                    {
                        telegramDefinition.TelegramNumber = telegramNumber;
                    }
                    else
                    {
                        continue;
                    }

                    IEnumerable<XElement> fieldList = element.Element("FieldList").Elements("Field");

                    foreach (XElement fieldElement in fieldList)
                    {
                        TelegramField telegramField;
                        if (fieldElement.Element("FieldDescription")?.Value != "" && fieldElement.Element("FieldDescription")?.Value != null)
                        {
                            telegramField = telegramDefinition.AddField(fieldElement.Element("FieldDescription")?.Value);
                        }
                        else
                        {
                            telegramField = telegramDefinition.AddField("NoName");
                        }

                        telegramField.Datatype = Enum.TryParse(fieldElement.Element("Datatype")?.Value, out Datatype datatype) ? datatype : Datatype.DWord;

                        IEnumerable<XElement> valueList = fieldElement.Elements("Values");

                        foreach (XElement valueElement in valueList)
                        {
                            _logger.Debug("Value repetition: {0}", valueElement.Attribute("Repetition")?.Value ?? "1");

                            telegramField.ValueDataCharsCount = Int32.TryParse(valueElement.Element("CharsCount")?.Value, out int valueDataCharsCount) ? valueDataCharsCount : 1;

                            telegramField.ValueDataChars = valueElement.Element("Chars")?.Value;

                            telegramField.ValueDataDWord = Int32.TryParse(valueElement.Element("DWord")?.Value, out int valueDataDWord) ? valueDataDWord : 0;
                        }
                    }

                    TelegramDefinitions.Add(telegramDefinition);
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex, "Exception while parsing XML settings");
            }
            finally
            {
                SortTelegramDefinitions();
                TelegramDefinitionsChanged(this, null);
            }
        }

        /// <summary>
        /// The SaveSettings
        /// </summary>
        public void SaveSettings()
        {
            _ioService.ChooseSettingsFileSave();
            _logger.Debug("Serializing settings to {0}", _ioService.SettingsFile);

            if (_ioService.SettingsFile != null)
            {
                XElement xml = new XElement("TelegramTester",
                        new XAttribute("Version", version),
                        new XAttribute("Date", DateTime.Now.ToString("yyyyMMddHHmmss")),
                    new XElement("ApplicationColor", ApplicationColor),
                    new XElement("ApplicationStyle", ApplicationStyle),
                    new XElement("AutosendAck", AutosendAck),
                    new XElement("BigEndianEncoding", BigEndianEncoding),
                    new XElement("Simulation", Simulation),
                    new XElement("Language", Language),
                    new XElement("ActiveConnection", ActiveConnection),
                    new XElement("RemoteHost", RemoteHost),
                    new XElement("Port", Port),
                    new XElement("MaximumLogCount", MaximumLogCount),
                    new XElement("TelegramDefinitions",
                        from telegramDefinition in TelegramDefinitions
                        select new XElement("Telegram",
                                new XElement("Direction", telegramDefinition.Direction),
                                new XElement("TelegramDescription", telegramDefinition.TelegramDescription),
                                new XElement("TelegramNumber", telegramDefinition.TelegramNumber),
                                new XElement("IsAckTelegram", telegramDefinition.IsAckTelegram),
                                new XElement("FieldList",
                                    from fieldDefinition in telegramDefinition.FieldList
                                    select new XElement("Field",
                                        new XElement("FieldDescription", fieldDefinition.FieldDescription),
                                        new XElement("Datatype", fieldDefinition.Datatype),
                                        new XElement("Values",
                                        new XElement("CharsCount", fieldDefinition.ValueDataCharsCount),
                                        new XElement("Chars", fieldDefinition.ValueDataChars),
                                        new XElement("DWord", fieldDefinition.ValueDataDWord)
                                        )
                                    )
                                )
                        )
                    )
                );

                XDocument xDocument = new XDocument(xml);
                StringWriter stringWriter = new StringWriter();
                xDocument.Save(stringWriter);

                _ioService.WriteSettings(stringWriter.GetStringBuilder().ToString());
            }
        }

        /// <summary>
        /// The Sort
        /// </summary>
        public void SortTelegramDefinitions()
        {
            TelegramDefinition[] array = new TelegramDefinition[TelegramDefinitions.Count];
            TelegramDefinitions.CopyTo(array, 0);
            Array.Sort(array); //Sort telegram definitions by telegram number
            bool oldRaise = TelegramDefinitions.RaiseListChangedEvents;
            TelegramDefinitions.RaiseListChangedEvents = false; // Disable changed event
            try
            {
                TelegramDefinitions.Clear();
                foreach (TelegramDefinition item in array)
                {
                    TelegramDefinitions.Add(item);
                }
            }
            finally
            {
                TelegramDefinitions.RaiseListChangedEvents = oldRaise;
                TelegramDefinitions.ResetBindings();
            }
        }

        /// <summary>
        /// The TelegramDefinitionsChanged
        /// </summary>
        /// <param name="sender">The <see cref="object"/></param>
        /// <param name="e">The <see cref="NotifyCollectionChangedEventArgs"/></param>
        public void TelegramDefinitionsChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            TelegramDefinitionsSend.Clear();

            foreach (TelegramDefinition definition in TelegramDefinitions)
            {
                if (
                    (definition.Direction == Direction.ToPLC && Simulation == Simulation.Level2 && !definition.IsAckTelegram)
                    || (definition.Direction == Direction.ToLevel2 && Simulation == Simulation.PLC && !definition.IsAckTelegram)
                   )
                {
                    TelegramDefinitionsSend.Add(definition);
                }
            }
        }

        /// <summary>
        /// The ChangeStyle
        /// </summary>
        private void ChangeStyle()
        {
            ThemeManager.ChangeAppStyle(Application.Current,
                ThemeManager.GetAccent(ApplicationColor),
                ThemeManager.GetAppTheme(ApplicationStyle.ToString()));
        }

        /// <summary>
        /// The SetLanguage
        /// </summary>
        private void SetLanguage()
        {
            WPFLocalizeExtension.Engine.LocalizeDictionary.Instance.SetCurrentThreadCulture = true;
            WPFLocalizeExtension.Engine.LocalizeDictionary.Instance.Culture = new CultureInfo(Language);
        }

        #endregion
    }
}
