﻿//-----------------------------------------------------------------------
// <copyright file="TCPService.cs" company="Raphael Pala">
//     Copyright (c) Raphael Pala. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace Pala.TelegramTester
{
    using Caliburn.Micro;
    using NLog;
    using System;
    using System.Collections.Generic;
    using System.Collections.Specialized;

    /// <summary>
    /// Defines the <see cref="TCPService" />
    /// </summary>
    internal class TCPService : ITCPService, IDisposable
    {
        #region Fields

        /// <summary>
        /// Defines the 
        /// </summary>
        private static readonly Logger _logger = NLog.LogManager.GetCurrentClassLogger();

        /// <summary>
        /// Defines the 
        /// </summary>
        private static ISettingsService _settingsService;

        /// <summary>
        /// Defines the 
        /// </summary>
        private readonly IEventAggregator _eventAggregator;

        /// <summary>
        /// Defines the 
        /// </summary>
        private TCPConnection _tcpConnection = null;

        #endregion

        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="TCPService"/> class.
        /// </summary>
        /// <param name="eventAggregator">The <see cref="IEventAggregator"/></param>
        /// /// <param name="settingsService">The <see cref="ISettingsService"/></param>
        public TCPService(IEventAggregator eventAggregator, ISettingsService settingsService)
        {
            _eventAggregator = eventAggregator;
            _settingsService = settingsService;
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets the TCPConnection
        /// </summary>
        public TCPConnection TCPConnection { get => _tcpConnection; }

        #endregion

        #region Methods

        /// <summary>
        /// The GetInt
        /// </summary>
        /// <param name="integerValue">The <see cref="int"/></param>
        /// <returns>The <see cref="int"/></returns>
        public static int GetInt(int integerValue)
        {
            if (_settingsService.BigEndianEncoding && BitConverter.IsLittleEndian)
            {
                return integerValue.ToBigEndian();
            }
            else
            {
                return integerValue;
            }
        }

        /// <summary>
        /// The GetShort
        /// </summary>
        /// <param name="shortValue">The <see cref="short"/></param>
        /// <returns>The <see cref="short"/></returns>
        public static short GetShort(short shortValue)
        {
            if (_settingsService.BigEndianEncoding && BitConverter.IsLittleEndian)
            {
                return shortValue.ToBigEndian();
            }
            else
            {
                return shortValue;
            }
        }

        /// <summary>
        /// The GetUShort
        /// </summary>
        /// <param name="ushortValue">The <see cref="ushort"/></param>
        /// <returns>The <see cref="ushort"/></returns>
        public static ushort GetUShort(ushort ushortValue)
        {
            if (_settingsService.BigEndianEncoding && BitConverter.IsLittleEndian)
            {
                return ushortValue.ToBigEndian();
            }
            else
            {
                return ushortValue;
            }
        }

        /// <summary>
        /// The Dispose
        /// </summary>
        public void Dispose()
        {
            _tcpConnection.Dispose();
        }

        /// <summary>
        /// The GetTelegramAsByteArray
        /// </summary>
        /// <param name="telegramDefinition">The <see cref="TelegramDefinition"/></param>
        /// <returns>The <see cref="byte[]"/></returns>
        public byte[] GetTelegramAsByteArray(TelegramDefinition telegramDefinition)
        {
            if (telegramDefinition == null)
            {
                _logger.Debug("Telegram definition is null.");
                return new byte[0];
            }

            //Add all fields to telegram data
            List<object> dataList = new List<object>();

            foreach (TelegramField field in telegramDefinition.FieldList)
            {
                if (field.Datatype == Datatype.DWord)
                {
                    dataList.Add(field.ValueDataDWord);
                }
                else if (field.Datatype == Datatype.Chars)
                {
                    dataList.Add(field.ValueDataChars.PadRight(field.ValueDataCharsCount));
                }
                else if (field.Datatype == Datatype.Bitfield)
                {
                    dataList.Add(field.ValueDataDWord);
                }
            }

            //Return telegram as byte array
            Telegram telegram = new Telegram(telegramDefinition.Direction, telegramDefinition.TelegramNumber, dataList);
            return telegram.TelegramAsByteArray;
        }

        /// <summary>
        /// The ParseTelegram
        /// </summary>
        /// <param name="telegram">The <see cref="Telegram"/></param>
        /// <returns>The <see cref="List{ParsedTelegramField}"/></returns>
        public List<ParsedTelegramField> ParseTelegram(Telegram telegram)
        {
            _logger.Debug("Parsing telegram {0}", telegram.TelegramNumber);

            List<ParsedTelegramField> fieldList = new List<ParsedTelegramField>();

            //Get definition of received telegram
            TelegramDefinition telegramDefinition = _settingsService.FindTelegramDefinition(telegram.TelegramNumber);
            if (telegramDefinition == null)
            {
                _logger.Debug("Could not find telegram {0}", telegram.TelegramNumber);
                return fieldList;
            }

            //Initialize position in byte-stream
            int position = 0;

            //Walk through all fields and parse received raw data according to field definition
            foreach (TelegramField field in telegramDefinition.FieldList)
            {
                _logger.Debug("Parsing field: {0}", field.FieldDescription);

                //Create new telegram field
                ParsedTelegramField parsedTelegramField = new ParsedTelegramField
                {
                    FieldDescription = field.FieldDescription
                };

                //if field datatype is DWord
                if (field.Datatype == Datatype.DWord)
                {
                    //If raw data is not long enough (4 more bytes for DWord), return received field list
                    if (position + 4 > telegram.RawData.Length)
                    {
                        return fieldList;
                    }

                    //Parse field
                    parsedTelegramField.FieldValue = TCPService.GetInt(BitConverter.ToInt32(telegram.RawData, position)).ToString();

                    //Add field to list
                    fieldList.Add(parsedTelegramField);

                    //increment position in bytearray
                    position += 4;
                }
                //if field datatype is Bitfield
                else if (field.Datatype == Datatype.Bitfield)
                {
                    //If raw data is not long enough (4 more bytes for DWord), return received field list
                    if (position + 4 > telegram.RawData.Length)
                    {
                        return fieldList;
                    }

                    //Parse field
                    int dWordValue = TCPService.GetInt(BitConverter.ToInt32(telegram.RawData, position));

                    BitVector32 bitvector = new BitVector32(dWordValue);

                    parsedTelegramField.FieldValue = "0                                                                31" + Environment.NewLine;

                    for (double i = 0; i <= 31; i++)
                    {
                        int bitmask = (int)Math.Pow(2.0, i);
                        bool bitValue = bitvector[bitmask];

                        if (i % 8 == 0)
                        {
                            parsedTelegramField.FieldValue += " ";
                        }

                        parsedTelegramField.FieldValue += (bitValue ? "1" : "0");
                    }

                    //Add field to list
                    fieldList.Add(parsedTelegramField);

                    //increment position in bytearray
                    position += 4;
                }
                //if field type is char array
                else
                {
                    //If raw data is not long enough, return received field list
                    if (position + field.ValueDataCharsCount > telegram.RawData.Length)
                    {
                        return fieldList;
                    }

                    //Parse field
                    parsedTelegramField.FieldValue = System.Text.Encoding.UTF8.GetString(telegram.RawData, position, field.ValueDataCharsCount);

                    //Add field to list
                    fieldList.Add(parsedTelegramField);

                    //increment position in bytearray
                    position += field.ValueDataCharsCount;
                }
            }

            return fieldList;
        }

        /// <summary>
        /// The SendTelegram
        /// </summary>
        /// <param name="telegramDefinition">The <see cref="TelegramDefinition"/></param>
        public void SendTelegram(TelegramDefinition telegramDefinition)
        {
            if (telegramDefinition == null)
            {
                return;
            }

            _logger.Debug("Send telegram: {0}", telegramDefinition.TelegramNumber);

            //Add all fields to telegram data
            List<object> dataList = new List<object>();

            foreach (TelegramField field in telegramDefinition.FieldList)
            {
                _logger.Debug("Preparing field: {0}", field.FieldDescription);
                if (field.Datatype == Datatype.DWord)
                {
                    dataList.Add(field.ValueDataDWord);
                }
                else if (field.Datatype == Datatype.Chars)
                {
                    dataList.Add(field.ValueDataChars.PadRight(field.ValueDataCharsCount));
                }
                else if (field.Datatype == Datatype.Bitfield)
                {
                    dataList.Add(field.ValueDataDWord);
                }
            }

            //Send telegram
            Telegram telegram = new Telegram(Direction.Send, telegramDefinition.TelegramNumber, dataList);

            if (TCPConnection != null)
            {
                TCPConnection.SendTelegram(telegram.TelegramAsByteArray);
                _eventAggregator.PublishOnUIThread(telegram);
            }
        }

        /// <summary>
        /// The StartClient
        /// </summary>
        public void StartClient()
        {
            _logger.Debug("New TCP client requested.");
            _tcpConnection = new TCPConnection(_settingsService.RemoteHost, _settingsService.Port, _settingsService);
            IoC.BuildUp(_tcpConnection);
            _tcpConnection.StartTCPClient();
        }

        /// <summary>
        /// The StartServer
        /// </summary>
        public void StartServer()
        {
            _logger.Debug("New TCP server requested.");
            _tcpConnection = new TCPConnection(_settingsService.Port, _settingsService);
            IoC.BuildUp(_tcpConnection);
            _tcpConnection.StartTCPServer();
        }

        /// <summary>
        /// The StopConnection
        /// </summary>
        public void StopConnection()
        {
            _tcpConnection?.StopTCPConnection();
            _tcpConnection = null;
        }

        #endregion
    }
}
