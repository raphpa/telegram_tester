﻿//-----------------------------------------------------------------------
// <copyright file="Server.cs" company="Raphael Pala">
//     Copyright (c) Raphael Pala. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace Pala.TelegramTester
{
    using Caliburn.Micro;
    using NLog;
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Net;
    using System.Net.Sockets;
    using System.Threading;
    using System.Threading.Tasks;

    /// <summary>
    /// Defines the <see cref="TCPConnection" />
    /// </summary>
    public class TCPConnection : IDisposable
    {
        #region Fields

        /// <summary>
        /// Defines the 
        /// </summary>
        private static readonly Logger _logger = NLog.LogManager.GetCurrentClassLogger();

        /// <summary>
        /// Defines the 
        /// </summary>
        private static CancellationToken _token;

        /// <summary>
        /// Defines the 
        /// </summary>
        private static CancellationTokenSource _tokenSource;

        /// <summary>
        /// Defines the 
        /// </summary>
        private readonly int _port = 20000;

        /// <summary>
        /// Defines the _serverIpAddress
        /// </summary>
        private readonly String _serverAddress = null;

        /// <summary>
        /// Defines the _settingsService
        /// </summary>
        private readonly ISettingsService _settingsService;

        /// <summary>
        /// Defines the 
        /// </summary>
        private bool _isConnected = false;

        /// <summary>
        /// Defines the 
        /// </summary>
        private TcpListenerExtended _listener = null;

        /// <summary>
        /// Defines the 
        /// </summary>
        private NetworkStream _networkStream = null;

        /// <summary>
        /// Defines the 
        /// </summary>
        private TcpClient _tcpClient = null;

        /// <summary>
        /// Defines the disposedValue
        /// </summary>
        private bool disposedValue = false;

        /// <summary>
        /// Defines the 
        /// </summary>
        private Task task = null;

        #endregion

        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="TCPConnection"/> class.
        /// </summary>
        /// <param name="port">The <see cref="int"/></param>
        /// <param name="settingsService"></param>
        public TCPConnection(int port, ISettingsService settingsService)
        {
            _port = port;
            _settingsService = settingsService;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="TCPConnection"/> class.
        /// </summary>
        /// <param name="serverAddress">The <see cref="string"/></param>
        /// <param name="port">The <see cref="int"/></param>
        /// <param name="settingsService">The <see cref="ISettingsService"/></param>
        public TCPConnection(string serverAddress, int port, ISettingsService settingsService)
        {
            _serverAddress = serverAddress;
            _port = port;
            _settingsService = settingsService;
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets the EventAggregator
        /// </summary>
        public IEventAggregator EventAggregator { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether IsConnected
        /// </summary>
        public bool IsConnected
        {
            get => _isConnected;
            set
            {
                _isConnected = value;
                if (_isConnected)
                {
                    RemoteIPAddress = ((IPEndPoint)_tcpClient.Client.RemoteEndPoint).Address;
                    EventAggregator.PublishOnUIThread(ConnectionHandle.IsConnected);
                }
                else
                {
                    RemoteIPAddress = null;
                    EventAggregator.PublishOnUIThread(ConnectionHandle.IsNotConnected);
                }
            }
        }

        /// <summary>
        /// Gets or sets the RemoteHost
        /// </summary>
        public IPAddress RemoteIPAddress { get; private set; } = null;

        #endregion

        #region Methods

        /// <summary>
        /// The Dispose
        /// </summary>
        public void Dispose()
        {
            Dispose(true);
        }

        /// <summary>
        /// The SendAck
        /// </summary>
        /// <param name="telegramNumber">The <see cref="int"/></param>
        /// <param name="transmissionNumber"></param>
        public void SendAck(int telegramNumber, int transmissionNumber)
        {
            _logger.Debug("Send ACK telegram for telegram number: {0}, transmission number: {1}", telegramNumber, transmissionNumber);

            //Add received telegram number and transmission number to ACK telegram data
            List<object> dataList = new List<object>
                {
                    telegramNumber,
                    transmissionNumber
                };

            //Send ACK telegram
            int ackTelegramNumber = 0;
            if (_settingsService.Simulation == Simulation.Level2)
            {
                ackTelegramNumber = _settingsService.GetAckTelegram(Direction.ToPLC);
            }
            else if (_settingsService.Simulation == Simulation.PLC)
            {
                ackTelegramNumber = _settingsService.GetAckTelegram(Direction.ToLevel2);
            }

            if (ackTelegramNumber != 0)
            {
                Telegram ackTelegram = new Telegram(Direction.Send, ackTelegramNumber, dataList);
                SendTelegram(ackTelegram.TelegramAsByteArray);
                EventAggregator.PublishOnUIThread(ackTelegram);
                Telegram.SetNextTransmissionNumber();
            }
        }

        /// <summary>
        /// The SendTelegram
        /// </summary>
        /// <param name="byteArray">The <see cref="byte[]"/></param>
        public void SendTelegram(byte[] byteArray)
        {
            if (_networkStream != null)
            {
                _logger.Debug("Trying to send telegram. Network Stream: {0}, Length: {1}", _networkStream, byteArray.Length);
                _networkStream.Write(byteArray, 0, byteArray.Length);
            }
        }

        /// <summary>
        /// The StartTCPClient
        /// </summary>
        public void StartTCPClient()
        {
            _tokenSource = new CancellationTokenSource();
            _token = _tokenSource.Token;

            //Start connecting to server
            _logger.Debug("Starting client, connect to {0} on port {1}", _serverAddress, _port);

            task = Task.Run(() => RunClient());
        }

        /// <summary>
        /// The StartTCPServer
        /// </summary>
        public void StartTCPServer()
        {
            _tokenSource = new CancellationTokenSource();
            _token = _tokenSource.Token;

            //Start listening for incoming connections
            _logger.Debug("Starting server, listener on port: {0}", _port);
            _listener = new TcpListenerExtended(IPAddress.Any, _port);

            //Run task waiting on TCP connection
            task = Task.Run(() => RunServer());
        }

        /// <summary>
        /// The StopTCPConnection
        /// </summary>
        public void StopTCPConnection()
        {
            _logger.Debug("Stopping TCP service");

            //Request cancellation
            _tokenSource.Cancel();

            _networkStream?.Close();
            _tcpClient?.Close();
        }

        /// <summary>
        /// The Dispose
        /// </summary>
        /// <param name="disposing">The <see cref="bool"/></param>
        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    _networkStream.Close();
                    _tcpClient.Close();
                }

                disposedValue = true;
            }
        }

        /// <summary>
        /// The ReadData
        /// </summary>
        /// <param name="networkStream">The <see cref="NetworkStream"/></param>
        private void ReadData(NetworkStream networkStream)
        {
            try
            {
                //Cancellation requested, return
                if (_token.IsCancellationRequested)
                {
                    _logger.Debug("Reading cancelled. Returning.");
                    return;
                }

                _logger.Debug("Blocking. Waiting for telegram length data.");
                byte[] byteArray = new byte[1000];
                byte[] receivedRawData;

                int dataLength = 0;

                //Wait for length received before timeout occurs.
                while (dataLength < 4)
                {
                    if (_token.IsCancellationRequested)
                    {
                        //Connection is closed.
                        _logger.Debug("Connection was closed. Return to listening for connection.");
                        IsConnected = false;
                        return;
                    }

                    int receivedLength = networkStream.Read(byteArray, dataLength, 4 - dataLength);
                    if (receivedLength <= 0)
                    {
                        //Connection is closed.
                        _logger.Debug("Connection was closed. Return to listening for connection.");
                        IsConnected = false;
                        return;
                    }
                    _logger.Debug("Received {0} bytes.", receivedLength);
                    dataLength += receivedLength;
                }

                //Telegram length received. Decode telegram length
                //Read telegram length (Bytes 0-3)
                int telegramLength = TCPService.GetInt(BitConverter.ToInt32(byteArray, 0));
                _logger.Debug("Telegram length received. Length: {0}", telegramLength);
                EventAggregator.PublishOnUIThread(telegramLength);

                //Create telegram
                Telegram telegram = new Telegram(Direction.Receive);

                //Check if telegram length is within parameters (between 8 and 2000 bytes)
                //If not, reset connection
                if (telegramLength < 8 || telegramLength > 2000)
                {
                    _logger.Debug("Telegram length not ok. Restart connection");
                    IsConnected = false;

                    receivedRawData = new byte[4];
                    Array.Copy(byteArray, receivedRawData, receivedRawData.Length);
                    telegram.ReceivedRawTelegram = receivedRawData;
                    EventAggregator.PublishOnUIThread(telegram);

                    return;
                }

                //Length ok, read rest of telegram (telegramLength bytes read)
                _logger.Debug("Blocking. Waiting for telegram data.");

                //Wait for (telegramLength) bytes of header received before timeout occurs.
                while (dataLength < telegramLength)
                {
                    int receivedLength = networkStream.Read(byteArray, dataLength, telegramLength - dataLength);
                    if (receivedLength <= 0)
                    {
                        //Connection is closed.
                        _logger.Debug("Connection was closed. Return to listening for connection.");
                        IsConnected = false;
                        return;
                    }

                    _logger.Debug("Received {0} bytes.", receivedLength);
                    dataLength += receivedLength;
                }

                //Telegram received. Decode message ID (Bytes 4-7)
                int telegramNumber = TCPService.GetInt(BitConverter.ToInt32(byteArray, 4));
                _logger.Debug("Message ID received: {0}", telegramNumber);
                telegram.TelegramNumber = telegramNumber;

                //Check if telegram number is within parameters (between 1000 and 3999)
                //If not, reset connection
                if (telegramNumber < 1000 || telegramNumber > 3999)
                {
                    _logger.Debug("Telegram number not ok. Restart connection");
                    IsConnected = false;

                    receivedRawData = new byte[dataLength];
                    Array.Copy(byteArray, receivedRawData, receivedRawData.Length);
                    telegram.ReceivedRawTelegram = receivedRawData;
                    EventAggregator.PublishOnUIThread(telegram);

                    return;
                }

                //Decode transmission number (Bytes 8-11)
                int transmissionNumber = TCPService.GetInt(BitConverter.ToInt32(byteArray, 8));
                _logger.Debug("Transmission number received: {0}", transmissionNumber);
                telegram.TransmissionNumber = transmissionNumber;

                //Check if transmission number is within parameters
                //If not, reset connection
                if (transmissionNumber > Int32.MaxValue || transmissionNumber < 1)
                {
                    _logger.Debug("Transmission number not ok. Restart connection");
                    IsConnected = false;

                    receivedRawData = new byte[dataLength];
                    Array.Copy(byteArray, receivedRawData, receivedRawData.Length);
                    telegram.ReceivedRawTelegram = receivedRawData;
                    EventAggregator.PublishOnUIThread(telegram);

                    return;
                }

                //Decode Timestamp (Bytes 12-27)
                telegram.TimeStamp = System.Text.Encoding.UTF8.GetString(byteArray, 12, 16);
                _logger.Debug("Timestamp received: {0}", telegram.TimeStamp);

                //Add raw data to telegram (Bytes 8-dataLength)
                byte[] rawData = new byte[dataLength - 8];
                Array.Copy(byteArray, 28, rawData, 0, rawData.Length);
                telegram.RawData = rawData;
                _logger.Debug("Raw data received: {0}", BitConverter.ToString(rawData));

                receivedRawData = new byte[telegramLength];
                Array.Copy(byteArray, receivedRawData, receivedRawData.Length);
                telegram.ReceivedRawTelegram = receivedRawData;

                _logger.Debug("Telegram received correctly.");
                EventAggregator.PublishOnUIThread(telegram);
            }
            catch (IOException exception)
            {
                switch (exception.InnerException)
                {
                    case SocketException socketException:
                        switch (socketException.ErrorCode)
                        {
                            case (int)SocketError.TimedOut:
                                _logger.Debug("Timeout. Back to waiting for header.");
                                return;
                            case (int)SocketError.ConnectionReset:
                                _logger.Debug("Connection was reset by partner while reading data.");
                                IsConnected = false;
                                return;
                            default:
                                _logger.Error(socketException, "Socket Exception. Return to listening for connection. Errorcode: {0}", socketException.ErrorCode);
                                IsConnected = false;
                                return;
                        }
                    default:
                        _logger.Error(exception.InnerException, "IO Exception. Return to listening for connection.");
                        IsConnected = false;
                        return;
                }
            }
            catch (Exception exception)
            {
                _logger.Error(exception, "Exception. Return to listening for connection.");
                IsConnected = false;
                return;
            }
        }

        /// <summary>
        /// The RunClient
        /// </summary>
        private void RunClient()
        {
            _logger.Debug("Client Task running.");

            //Loop connecting to server
            while (!_token.IsCancellationRequested)
            {
                try
                {
                    EventAggregator.PublishOnUIThread(ConnectionHandle.IsConnecting);
                    _tcpClient = new TcpClient();
                    _tcpClient.ConnectAsync(_serverAddress, _port).Wait(_token);

                    if (_tcpClient.Connected)
                    {
                        //Connection is established
                        IsConnected = true;

                        _logger.Debug("Connection established.");

                        //Set connection parameters
                        _tcpClient.ReceiveTimeout = 3000;
                        _networkStream = _tcpClient.GetStream();

                        //Read from connection
                        while (!_token.IsCancellationRequested && IsConnected)
                        {
                            ReadData(_networkStream);
                        }

                        //Cancellation requested, stop tcp connection
                        _logger.Debug("Stopping TCP connection");

                        _networkStream?.Close();
                        _networkStream = null;
                        _tcpClient?.Close();
                        IsConnected = false;

                        _logger.Debug("Stream closed.");

                        //If no complete cancellation is requested, start listener again
                        if (!_token.IsCancellationRequested)
                        {
                            _logger.Debug("No complete cancellation requested. Back to connecting.");
                        }
                    }
                }
                catch (AggregateException exception)
                {
                    SocketException socketException = (SocketException)exception.InnerException;

                    switch (socketException.ErrorCode)
                    {
                        case (int)SocketError.AddressAlreadyInUse:
                            _logger.Debug("Port is already in use.");
                            EventAggregator.PublishOnUIThread(ErrorHandle.PortInUse);
                            break;
                        case (int)SocketError.AccessDenied:
                            _logger.Debug("Port access denied.");
                            EventAggregator.PublishOnUIThread(ErrorHandle.PortInUse);
                            break;
                        case (int)SocketError.ConnectionRefused:
                            _logger.Debug("Connection refused.");
                            break;
                        case (int)SocketError.ConnectionReset:
                            _logger.Debug("Connection was reset by partner.");
                            break;
                        default:
                            _logger.Error(exception, "Socket Exception while connecting. Error Code: {0}", socketException.ErrorCode);
                            break;
                    }
                }
                catch (OperationCanceledException)
                {
                    _logger.Debug("Connection was cancelled.");
                }
                catch (NullReferenceException)
                {
                    _logger.Debug("Null reference exception. TcpClient already closed");
                }
                finally
                {
                    _networkStream?.Close();
                    _networkStream = null;
                    _tcpClient?.Close();
                    IsConnected = false;
                    EventAggregator.PublishOnUIThread(ConnectionHandle.IsNotWaiting);
                }

                _logger.Debug("Is Token cancellation requested: {0}", _token.IsCancellationRequested);
            }

            //Cancellation requested, stop listener
            _logger.Debug("Cancellation requested. Stopping connection.");

            EventAggregator.PublishOnUIThread(ConnectionHandle.IsNotWaiting);
        }

        /// <summary>
        /// The RunServer
        /// </summary>
        private async void RunServer()
        {
            _logger.Debug("Server Task running.");

            try
            {
                _listener.Start();
            }
            catch (System.Net.Sockets.SocketException exception)
            {
                switch (exception.ErrorCode)
                {
                    case (int)SocketError.AddressAlreadyInUse:
                        _logger.Debug("Port is already in use.");
                        EventAggregator.PublishOnUIThread(ErrorHandle.PortInUse);
                        break;
                    case (int)SocketError.AccessDenied:
                        _logger.Debug("Port access denied.");
                        EventAggregator.PublishOnUIThread(ErrorHandle.PortInUse);
                        break;
                    default:
                        _logger.Error(exception, "Socket Exception while starting listener. Error Code: {0}", exception.ErrorCode);
                        break;
                }
            }

            //Loop waiting for connection
            while (_listener.Active && !_token.IsCancellationRequested)
            {
                _logger.Debug("Waiting for TCP connection.");
                EventAggregator.PublishOnUIThread(ConnectionHandle.IsWaiting);
                try
                {
                    await Task.Delay(1000, _token).ConfigureAwait(false);
                }
                catch (System.Threading.Tasks.TaskCanceledException)
                {
                    _logger.Debug("TCPConnection Stop requested. Cancelling Task.");
                }

                if (_listener.Pending())
                {
                    //Connection accepted. AcceptTcpClient is blocking until connection is requested
                    _tcpClient = _listener.AcceptTcpClient();
                    _listener.Stop();
                    IsConnected = true;
                    _logger.Debug("Connection established to {0}. Stop listening.", RemoteIPAddress);

                    //Set connection parameters
                    _tcpClient.ReceiveTimeout = 3000;
                    _networkStream = _tcpClient.GetStream();

                    //Read from connection
                    while (!_token.IsCancellationRequested && IsConnected)
                    {
                        ReadData(_networkStream);
                    }

                    //Cancellation requested, stop tcp connection
                    _logger.Debug("Stopping TCP connection");
                    _networkStream?.Close();
                    _networkStream = null;
                    _tcpClient?.Close();
                    _tcpClient = null;

                    //If no complete cancellation is requested, start listener again
                    if (!_token.IsCancellationRequested)
                    {
                        _logger.Debug("No complete cancellation requested. Back to listening.");
                        _listener.Start();
                    }
                }
            }
            //Cancellation requested, stop listener
            _logger.Debug("Cancellation requested. Stopping listener.");
            IsConnected = false;
            _listener.Stop();
            EventAggregator.PublishOnUIThread(ConnectionHandle.IsNotWaiting);
        }

        #endregion
    }
}
