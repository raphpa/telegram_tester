﻿//-----------------------------------------------------------------------
// <copyright file="ParsedTelegramField.cs" company="Raphael Pala">
//     Copyright (c) Raphael Pala. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace Pala.TelegramTester
{
    using System;

    /// <summary>
    /// Defines the <see cref="ParsedTelegramField" />
    /// </summary>
    public class ParsedTelegramField
    {
        #region Properties

        /// <summary>
        /// Gets or sets the FieldDescription
        /// </summary>
        public string FieldDescription { get; set; }

        /// <summary>
        /// Gets or sets the ValueDataChars
        /// </summary>
        public String FieldValue { get; set; }

        #endregion
    }
}
