﻿//-----------------------------------------------------------------------
// <copyright file="Enums.cs" company="Raphael Pala">
//     Copyright (c) Raphael Pala. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace Pala.TelegramTester
{
    using System;
    using System.Collections;
    using System.ComponentModel;
    using System.Reflection;

    #region Enums

    /// <summary>
    /// Defines the Status
    /// </summary>
    public enum Status
    {
        NoStatus, Waiting, Connected, Connecting
    }

    /// <summary>
    /// Defines the ConnectionHandle
    /// </summary>
    public enum ConnectionHandle
    {
        IsWaiting, IsNotWaiting, IsConnected, IsNotConnected, IsConnecting
    }

    /// <summary>
    /// Defines the ErrorHandle
    /// </summary>
    public enum ErrorHandle
    {
        PortInUse
    }

    /// <summary>
    /// Defines the Style
    /// </summary>
    public enum Style
    {
        BaseLight, BaseDark
    }

    /// <summary>
    /// Defines the Datatype
    /// </summary>
    public enum Datatype
    {
        DWord,
        Chars,
        Bitfield,
        [Browsable(false)]
        RepeatGroupStart,
        [Browsable(false)]
        RepeatGroupEnd
    }

    /// <summary>
    /// Defines the Direction
    /// </summary>
    public enum Direction
    {
        ToPLC,
        ToLevel2,
        [Browsable(false)]
        Send,
        [Browsable(false)]
        Receive,
        [Browsable(false)]
        Internal
    }

    public enum Simulation
    {
        PLC,
        Level2
    }

    public enum Settings
    {
        Load, Save, Reload
    }

    #endregion

    /// <summary>
    /// Defines the <see cref="EnumerationManager" />
    /// </summary>
    public static class EnumerationManager
    {
        #region Methods

        /// <summary>
        /// The GetValues
        /// </summary>
        /// <param name="enumeration">The <see cref="Type"/></param>
        /// <returns>The <see cref="Array"/></returns>
        public static Array GetValues(Type enumeration)
        {
            Array wArray = Enum.GetValues(enumeration);
            ArrayList wFinalArray = new ArrayList();
            foreach (Enum wValue in wArray)
            {
                FieldInfo fi = enumeration.GetField(wValue.ToString());
                if (fi != null)
                {
                    BrowsableAttribute[] wBrowsableAttributes = fi.GetCustomAttributes(typeof(BrowsableAttribute), true) as BrowsableAttribute[];
                    if (wBrowsableAttributes.Length > 0)
                    {
                        //  If the Browsable attribute is false
                        if (!wBrowsableAttributes[0].Browsable)
                        {
                            // Do not add the enumeration to the list.
                            continue;
                        }
                    }

                    DescriptionAttribute[] wDescriptions = fi.GetCustomAttributes(typeof(DescriptionAttribute), true) as DescriptionAttribute[];
                    if (wDescriptions.Length > 0)
                    {
                        wFinalArray.Add(wDescriptions[0].Description);
                    }
                    else
                    {
                        wFinalArray.Add(wValue);
                    }
                }
            }

            return wFinalArray.ToArray();
        }

        #endregion
    }
}
