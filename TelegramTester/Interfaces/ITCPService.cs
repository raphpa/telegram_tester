﻿//-----------------------------------------------------------------------
// <copyright file="ITCPService.cs" company="Raphael Pala">
//     Copyright (c) Raphael Pala. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace Pala.TelegramTester
{
    using System.Collections.Generic;

    #region Interfaces

    /// <summary>
    /// Defines the <see cref="ITCPService" />
    /// </summary>
    public interface ITCPService
    {
        #region Properties

        /// <summary>
        /// Gets the TCPConnection
        /// </summary>
        TCPConnection TCPConnection { get; }

        #endregion

        #region Methods

        /// <summary>
        /// The GetTelegramAsByteArray
        /// </summary>
        /// <param name="telegramDefinition">The <see cref="TelegramDefinition"/></param>
        /// <returns>The <see cref="byte[]"/></returns>
        byte[] GetTelegramAsByteArray(TelegramDefinition telegramDefinition);

        /// <summary>
        /// The ParseTelegram
        /// </summary>
        /// <param name="telegram">The <see cref="Telegram"/></param>
        /// <returns>The <see cref="List{ParsedTelegramField}"/></returns>
        List<ParsedTelegramField> ParseTelegram(Telegram telegram);

        /// <summary>
        /// The SendTelegram
        /// </summary>
        /// <param name="telegramDefinition">The <see cref="TelegramDefinition"/></param>
        void SendTelegram(TelegramDefinition telegramDefinition);

        /// <summary>
        /// The StartClient
        /// </summary>
        void StartClient();

        /// <summary>
        /// The StartServer
        /// </summary>
        void StartServer();

        /// <summary>
        /// The StopConnection
        /// </summary>
        void StopConnection();

        #endregion
    }

    #endregion
}
