﻿//-----------------------------------------------------------------------
// <copyright file="IIOService.cs" company="Raphael Pala">
//     Copyright (c) Raphael Pala. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace Pala.TelegramTester
{
    #region Interfaces

    /// <summary>
    /// Defines the <see cref="IIOService" />
    /// </summary>
    public interface IIOService
    {
        #region Properties

        /// <summary>
        /// Gets the LogFile
        /// </summary>
        string LogFile { get; }

        /// <summary>
        /// Gets the SettingsFile
        /// </summary>
        string SettingsFile { get; }

        #endregion

        #region Methods

        /// <summary>
        /// The ChooseLogFileSave
        /// </summary>
        void ChooseLogFileSave();

        /// <summary>
        /// The ChooseFile
        /// </summary>
        void ChooseSettingsFileOpen();

        /// <summary>
        /// The ChooseFileSave
        /// </summary>
        void ChooseSettingsFileSave();

        /// <summary>
        /// The LoadText
        /// </summary>
        /// <returns>The <see cref="string"/></returns>
        string LoadSettings();

        /// <summary>
        /// The WriteLog
        /// </summary>
        /// <param name="text">The <see cref="string"/></param>
        void WriteLog(string text);

        /// <summary>
        /// The WriteText
        /// </summary>
        /// <param name="text">The <see cref="string"/></param>
        void WriteSettings(string text);

        #endregion
    }

    #endregion
}
