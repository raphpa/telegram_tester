﻿//-----------------------------------------------------------------------
// <copyright file="ISettingsService.cs" company="Raphael Pala">
//     Copyright (c) Raphael Pala. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace Pala.TelegramTester
{
    using System.Collections.ObjectModel;
    using System.Collections.Specialized;
    using System.ComponentModel;

    #region Interfaces

    /// <summary>
    /// Defines the <see cref="ISettingsService" />
    /// </summary>
    public interface ISettingsService
    {
        #region Properties

        /// <summary>
        /// Gets or sets a value indicating whether ActiveConnection
        /// </summary>
        bool ActiveConnection { get; set; }

        /// <summary>
        /// Gets or sets the ApplicationColor
        /// </summary>
        string ApplicationColor { get; set; }

        /// <summary>
        /// Gets or sets the ApplicationStyle
        /// </summary>
        Style ApplicationStyle { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether AutosendAck
        /// </summary>
        bool AutosendAck { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether BigEndianEncoding
        /// </summary>
        bool BigEndianEncoding { get; set; }

        /// <summary>
        /// Gets or sets the Language
        /// </summary>
        string Language { get; set; }

        /// <summary>
        /// Gets or sets the MaximumLogCount
        /// </summary>
        int MaximumLogCount { get; set; }

        /// <summary>
        /// Gets or sets the Port
        /// </summary>
        int Port { get; set; }

        /// <summary>
        /// Gets or sets the RemoteHost
        /// </summary>
        string RemoteHost { get; set; }

        /// <summary>
        /// Gets or sets the Simulation
        /// </summary>
        Simulation Simulation { get; set; }

        /// <summary>
        /// Gets the TelegramDefinitions
        /// </summary>
        BindingList<TelegramDefinition> TelegramDefinitions { get; }

        /// <summary>
        /// Gets the TelegramDefinitionsSend
        /// </summary>
        ObservableCollection<TelegramDefinition> TelegramDefinitionsSend { get; }

        #endregion

        #region Methods

        /// <summary>
        /// The AddTelegramDefinition
        /// </summary>
        /// <param name="telegramNumber">The <see cref="int"/></param>
        /// <returns>The <see cref="TelegramDefinition"/></returns>
        TelegramDefinition AddTelegramDefinition(int telegramNumber);

        /// <summary>                                                
        /// The DeleteTelegramDefinition
        /// </summary>
        /// <param name="telegramDefinition">The <see cref="TelegramDefinition"/></param>
        void DeleteTelegramDefinition(TelegramDefinition telegramDefinition);

        /// <summary>
        /// The FindTelegramDefinition
        /// </summary>
        /// <param name="telegramNumber">The <see cref="int"/></param>
        /// <returns>The <see cref="TelegramDefinition"/></returns>
        TelegramDefinition FindTelegramDefinition(int telegramNumber);

        /// <summary>
        /// The GetAckTelegram
        /// </summary>
        /// <param name="direction">The <see cref="Direction"/></param>
        /// <returns>The <see cref="int"/></returns>
        int GetAckTelegram(Direction direction);

        /// <summary>
        /// The LoadSettings
        /// </summary>
        void LoadSettings();

        /// <summary>
        /// The SaveSettings
        /// </summary>
        void SaveSettings();

        /// <summary>
        /// The SortTelegramDefinitions
        /// </summary>
        void SortTelegramDefinitions();

        /// <summary>
        /// The TelegramDefinitionsChanged
        /// </summary>
        /// <param name="sender">The <see cref="object"/></param>
        /// <param name="e">The <see cref="NotifyCollectionChangedEventArgs"/></param>
        void TelegramDefinitionsChanged(object sender, NotifyCollectionChangedEventArgs e);

        #endregion
    }

    #endregion
}
