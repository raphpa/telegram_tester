﻿//-----------------------------------------------------------------------
// <copyright file="HomeView.xaml.cs" company="Raphael Pala">
//     Copyright (c) Raphael Pala. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace Pala.TelegramTester
{
    using System.Windows;
    using System.Windows.Controls;

    /// <summary>
    /// Interaktionslogik für HomeView.xaml
    /// </summary>
    public partial class HomeView
    {
        #region Fields

        /// <summary>
        /// Defines the selectionLength
        /// </summary>
        private int selectionLength;

        /// <summary>
        /// Defines the selectionStart
        /// </summary>
        private int selectionStart;

        #endregion

        #region Constructors

        /// <summary>
        /// Initializes a new instance of the HomeView class.
        /// </summary>
        public HomeView()
        {
            InitializeComponent();
        }

        #endregion

        #region Methods

        /// <summary>
        /// The NoLostFocus
        /// </summary>
        /// <param name="sender">The <see cref="object"/></param>
        /// <param name="e">The <see cref="RoutedEventArgs"/></param>
        private void NoLostFocus(object sender, RoutedEventArgs e)
        {
            // When the RichTextBox loses focus the user can no longer see the selection.
            // This is a hack to make the RichTextBox think it did not lose focus.
            e.Handled = true;
        }

        /// <summary>
        /// The SelectionChanged
        /// </summary>
        /// <param name="sender">The <see cref="object"/></param>
        /// <param name="e">The <see cref="RoutedEventArgs"/></param>
        private void SelectionChanged(object sender, RoutedEventArgs e)
        {
            var textBox = sender as TextBox;
            selectionStart = textBox.SelectionStart;
            selectionLength = textBox.SelectionLength;
        }

        /// <summary>
        /// The TextChanged
        /// </summary>
        /// <param name="sender">The <see cref="object"/></param>
        /// <param name="e">The <see cref="TextChangedEventArgs"/></param>
        private void TextChanged(object sender, TextChangedEventArgs e)
        {
            if (selectionLength > 0)
            {
                var textBox = sender as TextBox;
                textBox.SelectionStart = selectionStart;
                textBox.SelectionLength = selectionLength;
            }
        }

        #endregion
    }
}
