﻿//-----------------------------------------------------------------------
// <copyright file="LoggingView.xaml.cs" company="Raphael Pala">
//     Copyright (c) Raphael Pala. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace Pala.TelegramTester
{
    /// <summary>
    /// Interaktionslogik für LoggingView.xaml
    /// </summary>
    public partial class LoggingView
    {
        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="LoggingView"/> class.
        /// </summary>
        public LoggingView()
        {
            InitializeComponent();
        }

        #endregion
    }
}
