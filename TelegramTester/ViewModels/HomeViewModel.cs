﻿//-----------------------------------------------------------------------
// <copyright file="HomeViewModel.cs" company="Raphael Pala">
//     Copyright (c) Raphael Pala. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

/// <summary>
/// ViewModel for Main View
/// </summary>

namespace Pala.TelegramTester
{
    using Caliburn.Micro;
    using NLog;
    using System;
    using System.Collections.ObjectModel;
    using System.Collections.Specialized;
    using System.ComponentModel;
    using System.Timers;

    /// <summary>
    /// This class contains properties that the main View can data bind to.
    /// </summary>
    public sealed class HomeViewModel : Screen, ITextBoxSendController, ITextBoxReceiveController, IDisposable, IHandle<ConnectionHandle>, IHandle<ErrorHandle>, IHandle<int>, IHandle<Telegram>, IHandle<Settings>
    {
        /// <summary>
        /// Defines the 
        /// </summary>
        private static readonly Logger _logger = NLog.LogManager.GetCurrentClassLogger();

        /// <summary>
        /// Defines the 
        /// </summary>
        private static int _timeLastReceived = 0;

        /// <summary>
        /// Defines the 
        /// </summary>
        private readonly IEventAggregator _eventAggregator;

        /// <summary>
        /// Defines the 
        /// </summary>
        private readonly Timer _lastReceivedTimer;

        /// <summary>
        /// Defines the 
        /// </summary>
        private readonly ISettingsService _settingsService;

        /// <summary>
        /// Defines the 
        /// </summary>
        private readonly ITCPService _tcpService;

        /// <summary>
        /// Defines the 
        /// </summary>
        private readonly IWindowManager _windowManager;

        /// <summary>
        /// Defines the 
        /// </summary>
        private Status _currentStatus = Status.NoStatus;

        /// <summary>
        /// Defines the 
        /// </summary>
        private string _receiveData = "";

        private string _sendData = "";

        private TelegramDefinition _selectedSendTelegram;

        private TelegramField _selectedField;

        /// <summary>
        /// Defines the 
        /// </summary>
        private int _transmissionNumber = 0;

        /// <summary>
        /// Defines the 
        /// </summary>
        private bool _isOpen = false;

        /// <summary>
        /// Defines the 
        /// </summary>
        private int _receivedLength = 0;

        /// <summary>
        /// Defines the 
        /// </summary>
        private string _remoteIPAddress = "";

        /// <summary>
        /// Defines the
        /// </summary>
        private string _telegramDescription = "";

        /// <summary>
        /// Defines the 
        /// </summary>
        private int _telegramNumber = 0;

        /// <summary>
        /// Defines the 
        /// </summary>
        private string _timestamp = "";

        private short _flags = 0;

        public event SelectAllSendEventHandler SelectAllSend
        {
            add { }
            remove { }
        }

        public event SelectSendEventHandler SelectSend;

        public event SetFocusSendEventHandler SetFocusSend;

        public event SelectAllReceiveEventHandler SelectAllReceive
        {
            add { }
            remove { }
        }

        public event SelectReceiveEventHandler SelectReceive;

        public event SetFocusReceiveEventHandler SetFocusReceive;

        /// <summary>
        /// Initializes a new instance of the <see cref="HomeViewModel"/> class.
        /// </summary>
        /// <param name="tcpService">The <see cref="ITCPService"/></param>
        /// <param name="eventAggregator">The <see cref="IEventAggregator"/></param>
        /// <param name="windowManager">The <see cref="IWindowManager"/></param>
        /// /// <param name="settingsService">The <see cref="ISettingsService"/></param>
        public HomeViewModel(ITCPService tcpService, IEventAggregator eventAggregator, IWindowManager windowManager, ISettingsService settingsService)
        {
            _tcpService = tcpService;
            _windowManager = windowManager;
            _settingsService = settingsService;
            _eventAggregator = eventAggregator;
            _eventAggregator.Subscribe(this);

            _lastReceivedTimer = new Timer(1000);
            _lastReceivedTimer.Elapsed += (sender, e) => TimeLastReceived++;
        }

        /// <summary>
        /// Gets or sets a value indicating whether AutoSendAck
        /// </summary>
        public bool AutoSendAck
        {
            get => _settingsService.AutosendAck;
            set
            {
                _settingsService.AutosendAck = value;
                NotifyOfPropertyChange(() => AutoSendAck);
                NotifyOfPropertyChange(() => CanSendAck);
            }
        }

        /// <summary>
        /// Gets a value indicating whether CanSendAck
        /// </summary>
        public bool CanSendAck
        {
            get
            {
                if (AutoSendAck) return false;
                else return true;
            }
        }

        /// <summary>
        /// Gets or sets the CurrentStatus
        /// </summary>
        public Status CurrentStatus
        {
            get => _currentStatus;
            private set
            {
                _currentStatus = value;
                NotifyOfPropertyChange(() => CurrentStatus);
                NotifyOfPropertyChange(() => IsWaiting);
                NotifyOfPropertyChange(() => IsConnected);
            }
        }

        public string SendTelegramNumber
        {
            get;
            private set;
        }

        public string SendTransmissionNumber
        {
            get;
            private set;
        }

        public string SendLength
        {
            get;
            private set;
        }

        public bool ActiveConnection
        {
            get => _settingsService.ActiveConnection;
            set
            {
                _settingsService.ActiveConnection = value;
                NotifyOfPropertyChange(() => ActiveConnection);
                NotifyOfPropertyChange(() => CanEditHost);
            }
        }

        private int Clamp(int value, int min, int max)
        {
            return (value < min) ? min : (value > max) ? max : value;
        }

        /// <summary>
        /// Gets or sets the RemoteHost
        /// </summary>
        public String RemoteHost
        {
            get => _settingsService.RemoteHost;
            set
            {
                _logger.Debug("setting remote host address to: {0}", value);
                _settingsService.RemoteHost = value;
                NotifyOfPropertyChange(() => RemoteHost);
            }
        }

        public int IPAddressOctet1
        {
            //Split ip address and try to parse first octet
            get => Int32.TryParse(_settingsService.RemoteHost.Split('.')[0], out int octet) ? octet : 10;
            set
            {
                int octet = Clamp(value, 0, 254);
                _settingsService.RemoteHost = octet.ToString() + "." + IPAddressOctet2 + "." + IPAddressOctet3 + "." + IPAddressOctet4;
                NotifyOfPropertyChange(() => IPAddressOctet1);
            }
        }

        public int IPAddressOctet2
        {
            //Split ip address and try to parse second octet
            get => Int32.TryParse(_settingsService.RemoteHost.Split('.')[1], out int octet) ? octet : 1;
            set
            {
                int octet = Clamp(value, 0, 254);
                _settingsService.RemoteHost = IPAddressOctet1 + "." + octet.ToString() + "." + IPAddressOctet3 + "." + IPAddressOctet4;
                NotifyOfPropertyChange(() => IPAddressOctet2);
            }
        }

        public int IPAddressOctet3
        {
            //Split ip address and try to parse third octet
            get => Int32.TryParse(_settingsService.RemoteHost.Split('.')[2], out int octet) ? octet : 1;
            set
            {
                int octet = Clamp(value, 0, 254);
                _settingsService.RemoteHost = IPAddressOctet1 + "." + IPAddressOctet2 + "." + octet.ToString() + "." + IPAddressOctet4;
                NotifyOfPropertyChange(() => IPAddressOctet3);
            }
        }

        public int IPAddressOctet4
        {
            //Split ip address and try to parse fourth octet
            get => Int32.TryParse(_settingsService.RemoteHost.Split('.')[3], out int octet) ? octet : 1;
            set
            {
                int octet = Clamp(value, 0, 254);
                _settingsService.RemoteHost = IPAddressOctet1 + "." + IPAddressOctet2 + "." + IPAddressOctet3 + "." + octet.ToString();
                NotifyOfPropertyChange(() => IPAddressOctet4);
            }
        }

        /// <summary>
        /// Gets or sets the ReceiveData
        /// </summary>
        public string ReceiveData
        {
            get => _receiveData;
            set
            {
                _receiveData = value;
                NotifyOfPropertyChange(() => ReceiveData);
            }
        }

        /// <summary>
        /// Gets or sets the SendData
        /// </summary>
        public string SendData
        {
            get => _sendData;
            set
            {
                _sendData = value;
                NotifyOfPropertyChange(() => SendData);
            }
        }

        /// <summary>
        /// Gets or sets the TransmissionNumber
        /// </summary>
        public int TransmissionNumber
        {
            get => _transmissionNumber;
            set
            {
                _transmissionNumber = value;
                NotifyOfPropertyChange(() => TransmissionNumber);
            }
        }

        /// <summary>
        /// Gets a value indicating whether IsConnected
        /// </summary>
        public bool IsConnected { get => CurrentStatus == Status.Connected; }

        /// <summary>
        /// Gets or sets a value indicating whether IsOpen
        /// </summary>
        public bool IsOpen
        {
            get => _isOpen;
            set
            {
                _isOpen = value;
                NotifyOfPropertyChange(() => IsOpen);
                NotifyOfPropertyChange(() => CanEditHost);
                NotifyOfPropertyChange(() => CanSetActive);
            }
        }

        /// <summary>
        /// Gets a value indicating whether IsWaiting
        /// </summary>
        public bool IsWaiting { get => CurrentStatus == Status.Waiting || CurrentStatus == Status.Connecting; }

        public bool CanEditHost
        {
            get => !IsOpen && ActiveConnection;
        }

        public bool CanSetActive
        {
            get => !IsOpen;
        }

        /// <summary>
        /// Gets or sets TestValue. Calls notify.
        /// </summary>
        public int PortNumber
        {
            get => _settingsService.Port;
            set
            {
                _settingsService.Port = value;
                NotifyOfPropertyChange(() => PortNumber);
            }
        }

        /// <summary>
        /// Gets or sets the ReceivedLength
        /// </summary>
        public int ReceivedLength
        {
            get => _receivedLength;
            set
            {
                _receivedLength = value;
                NotifyOfPropertyChange(() => ReceivedLength);
            }
        }

        /// <summary>
        /// Gets or sets the RemoteHost
        /// </summary>
        public string RemoteIPAddress
        {
            get => _remoteIPAddress;
            private set
            {
                _remoteIPAddress = value;
                NotifyOfPropertyChange(() => RemoteIPAddress);
            }
        }

        /// <summary>
        /// Gets or sets the SelectedSendTelegram
        /// </summary>
        public TelegramDefinition SelectedSendTelegram
        {
            get => _selectedSendTelegram;
            set
            {
                _selectedSendTelegram = value;

                UpdateSendData(value);

                _logger.Debug("Send Data: {0}", SendData);

                NotifyOfPropertyChange(() => TelegramFields);
            }
        }

        public int ValueDataDWord
        {
            get
            {
                return _selectedField?.ValueDataDWord ?? 0;
            }
            set
            {
                _selectedField.ValueDataDWord = value;
                NotifyOfPropertyChange(() => ValueDataDWord);
                UpdateSendData(SelectedSendTelegram);
            }
        }

        public string ValueDataChars
        {
            get
            {
                return _selectedField?.ValueDataChars ?? "";
            }

            set
            {
                _selectedField.ValueDataChars = value;
                UpdateSendData(SelectedSendTelegram);
            }
        }

        private void UpdateSendData(TelegramDefinition telegramDefinition)
        {
            _logger.Debug("Updating Send Data");

            if (telegramDefinition == null)
            {
                telegramDefinition = SelectedSendTelegram;

                if (telegramDefinition == null)
                {
                    return;
                }
            }

            byte[] byteArray = _tcpService.GetTelegramAsByteArray(telegramDefinition);
            SendData = BitConverter.ToString(byteArray);

            SendTelegramNumber = telegramDefinition.TelegramNumber.ToString();
            SendTransmissionNumber = Telegram.CurrentTransmissionNumber.ToString();
            SendLength = byteArray.Length.ToString();
            SendTimestamp = Telegram.CurrentTimestamp;
            SendFlags = 0;
            SendTelegramDescription = telegramDefinition.TelegramDescription;

            NotifyOfPropertyChange(() => SendData);
            NotifyOfPropertyChange(() => SendTelegramNumber);
            NotifyOfPropertyChange(() => SendTransmissionNumber);
            NotifyOfPropertyChange(() => SendLength);
            NotifyOfPropertyChange(() => SendTimestamp);
            NotifyOfPropertyChange(() => SendTimestampFormatted);
            NotifyOfPropertyChange(() => SendFlags);
            NotifyOfPropertyChange(() => SendFlagsFormatted);
            NotifyOfPropertyChange(() => SendTelegramDescription);
        }

        public BindingList<TelegramField> TelegramFields { get => _selectedSendTelegram?.FieldList; }

        public TelegramField SelectedField
        {
            get => _selectedField;
            set
            {
                _selectedField = value;
                NotifyOfPropertyChange(() => SelectedField);
                NotifyOfPropertyChange(() => ValueDataDWord);
                NotifyOfPropertyChange(() => ValueDataChars);

                if (_selectedField != null)
                {
                    _logger.Debug("Field selected: {0}, Description: {1}", value, _selectedField.FieldDescription);

                    SelectFieldInSendWindow(value);
                }
            }
        }

        private void SelectFieldInSendWindow(TelegramField telegramField)
        {
            if (telegramField != null && _selectedSendTelegram != null)
            {
                //if telegram field is repeat group, end processing
                if (telegramField.Datatype == Datatype.RepeatGroupStart || telegramField.Datatype == Datatype.RepeatGroupEnd)
                {
                    return;
                }

                //Select text in send preview window
                //Get start position of field in telegram 
                int position = _selectedSendTelegram.GetFieldBytePosition(telegramField);

                //For every byte move selection start by 2 characters (hex ascii representation)
                //add another character every 2 bytes (for '-')
                position *= 3;

                //Get length of field in telegram
                int length = _selectedSendTelegram.GetFieldLength(telegramField);

                //For every byte add 2 characters (hex ascii representation), add another character every 2 bytes (for '-'), substract 1 for ending '-'
                length = (length * 3) - 1;

                //Select text in send preview window
                SelectSend?.Invoke(this, position, length);
            }
        }

        private void SelectRangeInSendWindow(int position, int length)
        {
            SelectSend?.Invoke(this, position, length);
        }

        public void SetTextboxSendFocus()
        {
            SetFocusSend?.Invoke(this);
        }

        private void SelectRangeInReceiveWindow(int position, int length)
        {
            SelectReceive?.Invoke(this, position, length);
        }

        public void SetTextboxReceiveFocus()
        {
            SetFocusReceive?.Invoke(this);
        }

        public void SetBitInBitfield(int bit, bool value)
        {
            if (SelectedField == null)
            {
                return;
            }

            _logger.Debug("Setting bit {0} to {1}", bit, value);

            BitVector32 bitvector = new BitVector32(ValueDataDWord);
            int bitmask = (int)Math.Pow(2.0, (double)bit);
            bitvector[bitmask] = value;

            ValueDataDWord = bitvector.Data;
        }

        /// <summary>
        /// Gets the TelegramDefinitionsSend
        /// </summary>
        public ObservableCollection<TelegramDefinition> TelegramDefinitionsSend
        {
            get => _settingsService.TelegramDefinitionsSend;
        }

        public Simulation Simulation
        {
            get => _settingsService.Simulation;
        }

        /// <summary>
        /// Gets or sets the TelegramDescription
        /// </summary>
        public string TelegramDescription
        {
            get => _telegramDescription;
            set
            {
                _telegramDescription = value;
                NotifyOfPropertyChange(() => TelegramDescription);
            }
        }

        public string SendTelegramDescription
        {
            get;
            private set;
        }

        public void SelectLengthInSendWindow()
        {
            SelectRangeInSendWindow(0, 11);
        }

        public void SelectTelegramNumberInSendWindow()
        {
            SelectRangeInSendWindow(12, 11);
        }

        public void SelectTransmissionNumberInSendWindow()
        {
            SelectRangeInSendWindow(24, 11);
        }

        public void SelectTimestampInSendWindow()
        {
            SelectRangeInSendWindow(36, 47);
        }

        public void SelectFlagsInSendWindow()
        {
            SelectRangeInSendWindow(18, 5);
        }

        public void SelectLengthInReceiveWindow()
        {
            SelectRangeInReceiveWindow(0, 11);
        }

        public void SelectTelegramNumberInReceiveWindow()
        {
            SelectRangeInReceiveWindow(12, 11);
        }

        public void SelectTransmissionNumberInReceiveWindow()
        {
            SelectRangeInReceiveWindow(24, 11);
        }

        public void SelectTimestampInReceiveWindow()
        {
            SelectRangeInReceiveWindow(36, 47);
        }

        public void SelectFlagsInReceiveWindow()
        {
            SelectRangeInReceiveWindow(18, 5);
        }

        /// <summary>
        /// Gets or sets the TelegramNumber
        /// </summary>
        public int TelegramNumber
        {
            get => _telegramNumber;
            set
            {
                _telegramNumber = value;
                NotifyOfPropertyChange(() => TelegramNumber);
            }
        }

        /// <summary>
        /// Gets or sets the TimeLastReceived
        /// </summary>
        public int TimeLastReceived
        {
            get => _timeLastReceived;
            set
            {
                _timeLastReceived = value;
                NotifyOfPropertyChange(() => TimeLastReceived);
            }
        }

        /// <summary>
        /// Gets or sets the Timestamp
        /// </summary>
        public string Timestamp
        {
            get => _timestamp;
            private set
            {
                _timestamp = value;
                NotifyOfPropertyChange(() => Timestamp);
                NotifyOfPropertyChange(() => TimestampFormatted);
            }
        }

        public short Flags
        {
            get => _flags;
            private set
            {
                _flags = value;
                NotifyOfPropertyChange(() => Flags);
            }
        }

        public String FlagsFormatted
        {
            get => _flags.ToString();
        }

        public short SendFlags
        {
            get; set;
        }

        public String SendFlagsFormatted
        {
            get => SendFlags.ToString();
        }

        public string SendTimestamp
        {
            get;
            private set;
        } = "";

        public string TimestampFormatted
        {
            get
            {
                if (_timestamp.Length >= 12)
                {
                    return $"{_timestamp.Substring(6, 2)}.{_timestamp.Substring(4, 2)}.{_timestamp.Substring(0, 4)} " +
                           $"{_timestamp.Substring(8, 2)}:{_timestamp.Substring(10, 2)}:{_timestamp.Substring(12, 2)}";
                }
                else
                {
                    return "";
                }
            }
        }

        public string SendTimestampFormatted
        {
            get
            {
                if (SendTimestamp.Length >= 12)
                {
                    return $"{SendTimestamp.Substring(6, 2)}.{SendTimestamp.Substring(4, 2)}.{SendTimestamp.Substring(0, 4)} " +
                           $"{SendTimestamp.Substring(8, 2)}:{SendTimestamp.Substring(10, 2)}:{SendTimestamp.Substring(12, 2)}";
                }
                else
                {
                    return "";
                }
            }
        }

        /// <summary>
        /// The Dispose
        /// </summary>
        public void Dispose()
        {
            _lastReceivedTimer.Close();
        }

        public void ShowParse()
        {
            _eventAggregator.PublishOnUIThread(Message.ShowParse);
        }

        /// <summary>
        /// The Handle
        /// </summary>
        /// <param name="message">The <see cref="ConnectionHandle"/></param>
        public void Handle(ConnectionHandle message)
        {
            if (message.Equals(ConnectionHandle.IsWaiting))
            {
                CurrentStatus = Status.Waiting;
            }
            if (message.Equals(ConnectionHandle.IsConnecting))
            {
                CurrentStatus = Status.Connecting;
            }
            else if (message.Equals(ConnectionHandle.IsNotWaiting))
            {
                CurrentStatus = Status.NoStatus;
            }
            else if (message.Equals(ConnectionHandle.IsConnected))
            {
                CurrentStatus = Status.Connected;
                if (_tcpService.TCPConnection != null)
                {
                    RemoteIPAddress = _tcpService.TCPConnection.RemoteIPAddress.ToString();
                }
                ResetTime();
                StartTime();
            }
            else if (message.Equals(ConnectionHandle.IsNotConnected))
            {
                CurrentStatus = Status.NoStatus;
                RemoteIPAddress = "";
                StopTime();
            }
        }

        /// <summary>
        /// The Handle
        /// </summary>
        /// <param name="message">The <see cref="ErrorHandle"/></param>
        public void Handle(ErrorHandle message)
        {
            if (message.Equals(ErrorHandle.PortInUse))
            {
                SetConnectionOpen(false);
                _eventAggregator.PublishOnUIThread(Message.PortAlreadyUsed);
            }
        }

        /// <summary>
        /// The Handle
        /// </summary>
        /// <param name="message">The <see cref="int"/></param>
        public void Handle(int message)
        {
            ReceivedLength = message;
        }

        /// <summary>
        /// The Handle
        /// </summary>
        /// <param name="message">The <see cref="Telegram"/></param>
        public void Handle(Telegram message)
        {
            if (message.Direction == Direction.Receive)
            {
                TelegramNumber = message.TelegramNumber;
                TransmissionNumber = message.TransmissionNumber;
                Timestamp = message.TimeStamp;
                Flags = message.Flags;
                ReceiveData = BitConverter.ToString(message.ReceivedRawTelegram);

                bool isAckTelegram = (_settingsService.Simulation == Simulation.Level2 && _settingsService.GetAckTelegram(Direction.ToLevel2) == TelegramNumber)
                    || (_settingsService.Simulation == Simulation.PLC && _settingsService.GetAckTelegram(Direction.ToPLC) == TelegramNumber);

                if (_tcpService.TCPConnection != null && AutoSendAck && !isAckTelegram)
                {
                    _logger.Debug("Autosending ACK.");
                    _tcpService.TCPConnection.SendAck(TelegramNumber, TransmissionNumber);
                }

                TelegramDefinition telegramDefinition = _settingsService.FindTelegramDefinition(message.TelegramNumber);
                if (telegramDefinition != null)
                {
                    TelegramDescription = telegramDefinition.TelegramDescription;
                }
                else
                {
                    TelegramDescription = "";
                }

                //only trigger parsing if view is active
                if (IsActive)
                {
                    _eventAggregator.PublishOnUIThread(Message.ParseReceived);
                }

                ResetTime();
            }
        }

        /// <summary>
        /// The Handle
        /// </summary>
        /// <param name="message">The <see cref="Settings"/></param>
        public void Handle(Settings message)
        {
            if (message.Equals(Settings.Reload))
            {
                _logger.Debug("Reload settings.");
                ReloadSettings();
            }
        }

        /// <summary>
        /// The LoadSettings
        /// </summary>
        public void ReloadSettings()
        {
            NotifyOfPropertyChange(() => AutoSendAck);
            NotifyOfPropertyChange(() => ActiveConnection);
            NotifyOfPropertyChange(() => PortNumber);
            NotifyOfPropertyChange(() => IPAddressOctet1);
            NotifyOfPropertyChange(() => IPAddressOctet2);
            NotifyOfPropertyChange(() => IPAddressOctet3);
            NotifyOfPropertyChange(() => IPAddressOctet4);
            NotifyOfPropertyChange(() => RemoteHost);
            NotifyOfPropertyChange(() => CanEditHost);
            NotifyOfPropertyChange(() => CanSetActive);
        }

        /// <summary>
        /// The ResetTime
        /// </summary>
        public void ResetTime()
        {
            TimeLastReceived = 0;
        }

        /// <summary>
        /// The SendAck
        /// </summary>
        public void SendAck()
        {
            bool isAckTelegram = (_settingsService.Simulation == Simulation.Level2 && _settingsService.GetAckTelegram(Direction.ToLevel2) == TelegramNumber)
                || (_settingsService.Simulation == Simulation.PLC && _settingsService.GetAckTelegram(Direction.ToPLC) == TelegramNumber);

            if (!isAckTelegram)
            {
                _logger.Debug("Sending ACK");
                _tcpService.TCPConnection?.SendAck(TelegramNumber, TransmissionNumber);
            }
        }

        /// <summary>
        /// The SendTelegram
        /// </summary>
        public void SendTelegram()
        {
            if (SelectedSendTelegram != null)
            {
                _logger.Debug("Sending telegram {0}", SelectedSendTelegram.TelegramNumber);
                _tcpService.SendTelegram(SelectedSendTelegram);
                Telegram.SetNextTransmissionNumber();
                UpdateSendData(SelectedSendTelegram);
            }
        }

        /// <summary>
        /// The SetConnectionOpen
        /// </summary>
        /// <param name="setConnectionOpen">The <see cref="bool"/></param>
        public void SetConnectionOpen(bool setConnectionOpen)
        {
            _logger.Debug("Set connection open: {0}", setConnectionOpen);

            IsOpen = setConnectionOpen;

            if (setConnectionOpen)
            {
                if (_settingsService.ActiveConnection)
                {
                    _tcpService.StartClient();
                }
                else
                {
                    _tcpService.StartServer();
                }
            }
            else
            {
                _tcpService.StopConnection();
            }
        }

        /// <summary>
        /// The StartTime
        /// </summary>
        public void StartTime()
        {
            _lastReceivedTimer.Start();
        }

        /// <summary>
        /// The StopTime
        /// </summary>
        public void StopTime()
        {
            _lastReceivedTimer.Stop();
        }
    }
}
