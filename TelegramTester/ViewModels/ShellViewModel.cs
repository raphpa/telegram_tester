﻿//-----------------------------------------------------------------------
// <copyright file="ShellViewModel.cs" company="Raphael Pala">
//     Copyright (c) Raphael Pala. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

/// <summary>
/// ViewModel for Main View
/// </summary>

namespace Pala.TelegramTester
{
    using Caliburn.Micro;
    using NLog;
    using System.Collections.Generic;

    #region Enums

    /// <summary>
    /// Defines the Message
    /// </summary>
    public enum Message
    {
        ParseReceived, ParseInternal, ShowParse, PortAlreadyUsed
    }

    #endregion

    /// <summary>
    /// Defines the <see cref="ShellViewModel" />
    /// </summary>
    internal sealed class ShellViewModel : Conductor<Screen>, IShell, IHandle<Message>, IHandle<Telegram>
    {
        #region Fields

        /// <summary>
        /// Defines the 
        /// </summary>
        private static readonly Logger _logger = NLog.LogManager.GetCurrentClassLogger();

        /// <summary>
        /// Defines the 
        /// </summary>
        private readonly IEventAggregator _eventAggregator;

        /// <summary>
        /// Defines the _settingsService
        /// </summary>
        private readonly ISettingsService _settingsService;

        /// <summary>
        /// Defines the _tcpService
        /// </summary>
        private readonly ITCPService _tcpService;

        /// <summary>
        /// Defines the 
        /// </summary>
        private Message _errorMessage;

        /// <summary>
        /// Defines the _parsedFieldList
        /// </summary>
        private List<ParsedTelegramField> _parsedFieldList;

        /// <summary>
        /// Defines the 
        /// </summary>
        private bool _showErrorMessage;

        /// <summary>
        /// Defines the _showParse
        /// </summary>
        private bool _showParse;

        #endregion

        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="ShellViewModel"/> class.
        /// </summary>
        /// <param name="eventAggregator">The <see cref="IEventAggregator"/></param>
        /// <param name="tcpService"></param>
        /// <param name="settingsService"></param>
        public ShellViewModel(IEventAggregator eventAggregator, ITCPService tcpService, ISettingsService settingsService)
        {
            _eventAggregator = eventAggregator;
            _eventAggregator.Subscribe(this);

            _tcpService = tcpService;
            _settingsService = settingsService;

            ShowHome();

            //Instantiate Logging view model
            IoC.Get<LoggingViewModel>();
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets the DisplayName
        /// </summary>
        public override string DisplayName { get => "TelegramTester"; }

        /// <summary>
        /// Gets or sets the Message
        /// </summary>
        public Message ErrorMessage
        {
            get => _errorMessage;
            private set
            {
                _errorMessage = value;
                NotifyOfPropertyChange(() => ErrorMessage);
            }
        }

        /// <summary>
        /// Gets or sets the ParsedFieldList
        /// </summary>
        public List<ParsedTelegramField> ParsedFieldList
        {
            get => _parsedFieldList;
            private set
            {
                _parsedFieldList = value;
                NotifyOfPropertyChange(() => ParsedFieldList);
                NotifyOfPropertyChange(() => ParsedTelegramDescription);
            }
        }

        /// <summary>
        /// Gets or sets the ParsedTelegramDescription
        /// </summary>
        public string ParsedTelegramDescription { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether ShowErrorMessage
        /// </summary>
        public bool ShowErrorMessage
        {
            get => _showErrorMessage;
            set
            {
                _showErrorMessage = value;
                NotifyOfPropertyChange(() => ShowErrorMessage);
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether ShowParse
        /// </summary>
        public bool ShowParse
        {
            get => _showParse;
            set
            {
                _showParse = value;
                NotifyOfPropertyChange(() => ShowParse);
            }
        }

        /// <summary>
        /// Gets or sets the LastInternalTelegram
        /// </summary>
        private Telegram LastInternalTelegram { get; set; }

        /// <summary>
        /// Gets or sets the LastReceivedTelegram
        /// </summary>
        private Telegram LastReceivedTelegram { get; set; }

        #endregion

        #region Methods

        /// <summary>
        /// The Handle
        /// </summary>
        /// <param name="message">The <see cref="ErrorMessage"/></param>
        public void Handle(Message message)
        {
            if (message == Message.ParseInternal)
            {
                if (LastInternalTelegram != null)
                {
                    ParsedTelegramDescription = LastInternalTelegram.TelegramNumber + " " + _settingsService.FindTelegramDefinition(LastInternalTelegram.TelegramNumber)?.TelegramDescription ?? "";
                    ParsedFieldList = _tcpService.ParseTelegram(LastInternalTelegram);
                }
            }
            else if (message == Message.ParseReceived)
            {
                if (LastReceivedTelegram != null)
                {
                    ParsedTelegramDescription = LastReceivedTelegram.TelegramNumber + " " + _settingsService.FindTelegramDefinition(LastReceivedTelegram.TelegramNumber)?.TelegramDescription ?? "";
                    ParsedFieldList = _tcpService.ParseTelegram(LastReceivedTelegram);
                }
            }
            else if (message == Message.ShowParse)
            {
                ShowParse = true;
            }
            else
            {
                ErrorMessage = message;
                ShowErrorMessage = true;
            }
        }

        /// <summary>
        /// The Handle
        /// </summary>
        /// <param name="message">The <see cref="Telegram"/></param>
        public void Handle(Telegram message)
        {
            //Parse received telegram and get field list
            if (message.Direction == Direction.Receive)
            {
                LastReceivedTelegram = message;
            }
            else if (message.Direction == Direction.Internal)
            {
                LastInternalTelegram = message;
            }
        }

        /// <summary>
        /// The LoadSettings
        /// </summary>
        public void LoadSettings()
        {
            _logger.Debug("Loading settings");
            IoC.Get<SettingsViewModel>();
            _eventAggregator.PublishOnUIThread(Settings.Load);
        }

        /// <summary>
        /// The SaveSettings
        /// </summary>
        public void SaveSettings()
        {
            _logger.Debug("Saving settings");
            IoC.Get<SettingsViewModel>();
            _eventAggregator.PublishOnUIThread(Settings.Save);
        }

        /// <summary>
        /// The ShowHome
        /// </summary>
        public void ShowHome()
        {
            _logger.Debug("Showing Home View");
            ActivateItem(IoC.Get<HomeViewModel>());
            _settingsService.TelegramDefinitionsChanged(this, null);
            _eventAggregator.PublishOnUIThread(Message.ParseReceived);
        }

        /// <summary>
        /// The ShowLogging
        /// </summary>
        public void ShowLogging()
        {
            _logger.Debug("Showing Logging View");
            ActivateItem(IoC.Get<LoggingViewModel>());
        }

        /// <summary>
        /// The ShowSettings
        /// </summary>
        public void ShowSettings()
        {
            _logger.Debug("Showing Settings View");
            ActivateItem(IoC.Get<SettingsViewModel>());
        }

        #endregion
    }
}
