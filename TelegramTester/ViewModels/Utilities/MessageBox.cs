﻿//-----------------------------------------------------------------------
// <copyright file="MessageBox.cs" company="Raphael Pala">
//     Copyright (c) Raphael Pala. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace Pala.TelegramTester
{
    using MahApps.Metro.Controls;
    using MahApps.Metro.Controls.Dialogs;
    using System.Threading.Tasks;
    using System.Windows;

    /// <summary>
    /// Defines the <see cref="MessageBox" />
    /// </summary>
    public static class MessageBox
    {
        #region Methods

        /// <summary>
        /// The ShowInputAsync
        /// </summary>
        /// <param name="title">The <see cref="string"/></param>
        /// <param name="message">The <see cref="string"/></param>
        /// <param name="settings">The <see cref="MetroDialogSettings"/></param>
        /// <returns>The <see cref="Task{string}"/></returns>
        public static Task<string> ShowInputAsync(string title, string message,
             MetroDialogSettings settings = null)
        {
            return ((MetroWindow)(Application.Current.MainWindow)).ShowInputAsync(title, message, settings);
        }

        /// <summary>
        /// The ShowMessageAsync
        /// </summary>
        /// <param name="title">The <see cref="string"/></param>
        /// <param name="message">The <see cref="string"/></param>
        /// <param name="style">The <see cref="MessageDialogStyle"/></param>
        /// <param name="settings">The <see cref="MetroDialogSettings"/></param>
        /// <returns>The <see cref="Task{MessageDialogResult}"/></returns>
        public static Task<MessageDialogResult> ShowMessageAsync(string title, string message,
            MessageDialogStyle style = MessageDialogStyle.Affirmative,
            MetroDialogSettings settings = null)
        {
            return ((MetroWindow)(Application.Current.MainWindow)).ShowMessageAsync(title, message, style, settings);
        }

        #endregion
    }
}
