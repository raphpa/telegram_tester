﻿//-----------------------------------------------------------------------
// <copyright file="TextBoxAttachSend.cs" company="Raphael Pala">
//     Copyright (c) Raphael Pala. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace Pala.TelegramTester
{
    using System;
    using System.Collections.Generic;
    using System.Windows;
    using System.Windows.Controls;

    #region Delegates

    /// <summary>
    /// The SelectAllEventHandler
    /// </summary>
    /// <param name="sender">The <see cref="ITextBoxSendController"/></param>
    public delegate void SelectAllSendEventHandler(ITextBoxSendController sender);

    /// <summary>
    /// The SelectSendEventHandler
    /// </summary>
    /// <param name="sender">The <see cref="ITextBoxSendController"/></param>
    /// <param name="start">The <see cref="int"/></param>
    /// <param name="length">The <see cref="int"/></param>
    public delegate void SelectSendEventHandler(ITextBoxSendController sender, int start, int length);

    /// <summary>
    /// The SetFocusSendEventHandler
    /// </summary>
    /// <param name="sender">The <see cref="ITextBoxSendController"/></param>
    public delegate void SetFocusSendEventHandler(ITextBoxSendController sender);

    #endregion

    #region Interfaces

    /// <summary>
    /// Defines the <see cref="ITextBoxSendController" />
    /// </summary>
    public interface ITextBoxSendController
    {
        #region Events

        /// <summary>
        /// Defines the SelectAll
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1009:DeclareEventHandlersCorrectly")]
        event SelectAllSendEventHandler SelectAllSend;

        /// <summary>
        /// Defines the SelectSend
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1009:DeclareEventHandlersCorrectly")]
        event SelectSendEventHandler SelectSend;

        /// <summary>
        /// Defines the SetFocusSend
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1009:DeclareEventHandlersCorrectly")]
        event SetFocusSendEventHandler SetFocusSend;

        #endregion
    }

    #endregion

    /// <summary>
    /// Defines the <see cref="TextBoxAttachSend" />
    /// </summary>
    public static class TextBoxAttachSend
    {
        #region Fields

        /// <summary>
        /// Defines the TextBoxControllerProperty
        /// </summary>
        public static readonly DependencyProperty TextBoxControllerProperty = DependencyProperty.RegisterAttached(
            "TextBoxController", typeof(ITextBoxSendController), typeof(TextBoxAttachSend),
            new FrameworkPropertyMetadata(null, OnTextBoxControllerChanged));

        /// <summary>
        /// Defines the elements
        /// </summary>
        private static readonly Dictionary<ITextBoxSendController, TextBox> elements = new Dictionary<ITextBoxSendController, TextBox>();

        #endregion

        #region Methods

        /// <summary>
        /// The GetTextBoxController
        /// </summary>
        /// <param name="element">The <see cref="UIElement"/></param>
        /// <returns>The <see cref="ITextBoxSendController"/></returns>
        public static ITextBoxSendController GetTextBoxController(UIElement element)
        {
            return (ITextBoxSendController)element.GetValue(TextBoxControllerProperty);
        }

        /// <summary>
        /// The SetTextBoxController
        /// </summary>
        /// <param name="element">The <see cref="UIElement"/></param>
        /// <param name="value">The <see cref="ITextBoxSendController"/></param>
        public static void SetTextBoxController(UIElement element, ITextBoxSendController value)
        {
            element.SetValue(TextBoxControllerProperty, value);
        }

        /// <summary>
        /// The OnTextBoxControllerChanged
        /// </summary>
        /// <param name="d">The <see cref="DependencyObject"/></param>
        /// <param name="e">The <see cref="DependencyPropertyChangedEventArgs"/></param>
        private static void OnTextBoxControllerChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var element = d as TextBox;
            if (element == null)
                throw new ArgumentNullException(nameof(d));

            if (e.OldValue is ITextBoxSendController oldController)
            {
                elements.Remove(oldController);
                oldController.SelectAllSend -= SelectAllSend;
                oldController.SelectSend -= SelectSend;
                oldController.SetFocusSend -= SetFocusSend;
            }

            if (e.NewValue is ITextBoxSendController newController)
            {
                elements.Clear();
                elements.Add(newController, element);
                newController.SelectAllSend += SelectAllSend;
                newController.SelectSend += SelectSend;
                newController.SetFocusSend += SetFocusSend;
            }
        }

        /// <summary>
        /// The SelectAll
        /// </summary>
        /// <param name="sender">The <see cref="ITextBoxSendController"/></param>
        private static void SelectAllSend(ITextBoxSendController sender)
        {
            if (!elements.TryGetValue(sender, out TextBox element))
            {
                throw new ArgumentException("sender");
            }

            element.Focus();
            element.SelectAll();
        }

        /// <summary>
        /// The SelectSend
        /// </summary>
        /// <param name="sender">The <see cref="ITextBoxSendController"/></param>
        /// <param name="start">The <see cref="int"/></param>
        /// <param name="length">The <see cref="int"/></param>
        private static void SelectSend(ITextBoxSendController sender, int start, int length)
        {
            if (!elements.TryGetValue(sender, out TextBox element))
            {
                throw new ArgumentException("sender");
            }

            element.Focus();
            element.Select(start, length);
        }

        /// <summary>
        /// The SetFocusSend
        /// </summary>
        /// <param name="sender">The <see cref="ITextBoxSendController"/></param>
        private static void SetFocusSend(ITextBoxSendController sender)
        {
            if (!elements.TryGetValue(sender, out TextBox element))
            {
                throw new ArgumentException("sender");
            }

            element.Focus();
        }

        #endregion
    }
}
