﻿//-----------------------------------------------------------------------
// <copyright file="TextBoxAttachReceive.cs" company="Raphael Pala">
//     Copyright (c) Raphael Pala. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace Pala.TelegramTester
{
    using System;
    using System.Collections.Generic;
    using System.Windows;
    using System.Windows.Controls;

    #region Delegates

    /// <summary>
    /// The SelectAllEventHandler
    /// </summary>
    /// <param name="sender">The <see cref="ITextBoxReceiveController"/></param>
    public delegate void SelectAllReceiveEventHandler(ITextBoxReceiveController sender);

    /// <summary>
    /// The SelectReceiveEventHandler
    /// </summary>
    /// <param name="sender">The <see cref="ITextBoxReceiveController"/></param>
    /// <param name="start">The <see cref="int"/></param>
    /// <param name="length">The <see cref="int"/></param>
    public delegate void SelectReceiveEventHandler(ITextBoxReceiveController sender, int start, int length);

    /// <summary>
    /// The SetFocusReceiveEventHandler
    /// </summary>
    /// <param name="sender">The <see cref="ITextBoxReceiveController"/></param>
    public delegate void SetFocusReceiveEventHandler(ITextBoxReceiveController sender);

    #endregion

    #region Interfaces

    /// <summary>
    /// Defines the <see cref="ITextBoxReceiveController" />
    /// </summary>
    public interface ITextBoxReceiveController
    {
        #region Events

        /// <summary>
        /// Defines the SelectAll
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1009:DeclareEventHandlersCorrectly")]
        event SelectAllReceiveEventHandler SelectAllReceive;

        /// <summary>
        /// Defines the SelectReceive
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1009:DeclareEventHandlersCorrectly")]
        event SelectReceiveEventHandler SelectReceive;

        /// <summary>
        /// Defines the SetFocusReceive
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1009:DeclareEventHandlersCorrectly")]
        event SetFocusReceiveEventHandler SetFocusReceive;

        #endregion
    }

    #endregion

    /// <summary>
    /// Defines the <see cref="TextBoxAttachReceive" />
    /// </summary>
    public static class TextBoxAttachReceive
    {
        #region Fields

        /// <summary>
        /// Defines the TextBoxControllerProperty
        /// </summary>
        public static readonly DependencyProperty TextBoxControllerProperty = DependencyProperty.RegisterAttached(
            "TextBoxController", typeof(ITextBoxReceiveController), typeof(TextBoxAttachReceive),
            new FrameworkPropertyMetadata(null, OnTextBoxControllerChanged));

        /// <summary>
        /// Defines the elements
        /// </summary>
        private static readonly Dictionary<ITextBoxReceiveController, TextBox> elements = new Dictionary<ITextBoxReceiveController, TextBox>();

        #endregion

        #region Methods

        /// <summary>
        /// The GetTextBoxController
        /// </summary>
        /// <param name="element">The <see cref="UIElement"/></param>
        /// <returns>The <see cref="ITextBoxReceiveController"/></returns>
        public static ITextBoxReceiveController GetTextBoxController(UIElement element)
        {
            return (ITextBoxReceiveController)element.GetValue(TextBoxControllerProperty);
        }

        /// <summary>
        /// The SetTextBoxController
        /// </summary>
        /// <param name="element">The <see cref="UIElement"/></param>
        /// <param name="value">The <see cref="ITextBoxReceiveController"/></param>
        public static void SetTextBoxController(UIElement element, ITextBoxReceiveController value)
        {
            element.SetValue(TextBoxControllerProperty, value);
        }

        /// <summary>
        /// The OnTextBoxControllerChanged
        /// </summary>
        /// <param name="d">The <see cref="DependencyObject"/></param>
        /// <param name="e">The <see cref="DependencyPropertyChangedEventArgs"/></param>
        private static void OnTextBoxControllerChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var element = d as TextBox;
            if (element == null)
                throw new ArgumentNullException(nameof(d));

            if (e.OldValue is ITextBoxReceiveController oldController)
            {
                elements.Remove(oldController);
                oldController.SelectAllReceive -= SelectAllReceive;
                oldController.SelectReceive -= SelectReceive;
                oldController.SetFocusReceive -= SetFocusReceive;
            }

            if (e.NewValue is ITextBoxReceiveController newController)
            {
                elements.Clear();
                elements.Add(newController, element);
                newController.SelectAllReceive += SelectAllReceive;
                newController.SelectReceive += SelectReceive;
                newController.SetFocusReceive += SetFocusReceive;
            }
        }

        /// <summary>
        /// The SelectAll
        /// </summary>
        /// <param name="sender">The <see cref="ITextBoxReceiveController"/></param>
        private static void SelectAllReceive(ITextBoxReceiveController sender)
        {
            if (!elements.TryGetValue(sender, out TextBox element))
            {
                throw new ArgumentException("sender");
            }

            element.Focus();
            element.SelectAll();
        }

        /// <summary>
        /// The SelectReceive
        /// </summary>
        /// <param name="sender">The <see cref="ITextBoxReceiveController"/></param>
        /// <param name="start">The <see cref="int"/></param>
        /// <param name="length">The <see cref="int"/></param>
        private static void SelectReceive(ITextBoxReceiveController sender, int start, int length)
        {
            if (!elements.TryGetValue(sender, out TextBox element))
            {
                throw new ArgumentException("sender");
            }

            element.Focus();
            element.Select(start, length);
        }

        /// <summary>
        /// The SetFocusReceive
        /// </summary>
        /// <param name="sender">The <see cref="ITextBoxReceiveController"/></param>
        private static void SetFocusReceive(ITextBoxReceiveController sender)
        {
            if (!elements.TryGetValue(sender, out TextBox element))
            {
                throw new ArgumentException("sender");
            }

            element.Focus();
        }

        #endregion
    }
}
