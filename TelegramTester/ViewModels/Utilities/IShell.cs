﻿//-----------------------------------------------------------------------
// <copyright file="ishell.cs" company="Raphael Pala">
//     Copyright (c) Raphael Pala. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace Pala.TelegramTester
{
    #region Interfaces

    /// <summary>
    /// Defines the <see cref="IShell" />
    /// </summary>
    public interface IShell
    {
    }

    #endregion
}
