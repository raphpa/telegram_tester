﻿//-----------------------------------------------------------------------
// <copyright file="LocalizationProvider.cs" company="Raphael Pala">
//     Copyright (c) Raphael Pala. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace Pala.TelegramTester
{
    using System.Reflection;
    using WPFLocalizeExtension.Extensions;

    /// <summary>
    /// Defines the <see cref="LocalizationProvider" />
    /// </summary>
    public static class LocalizationProvider
    {
        #region Methods

        /// <summary>
        /// The GetLocalizedValue
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="key">The <see cref="string"/></param>
        /// <returns>The <see cref="T"/></returns>
        public static T GetLocalizedValue<T>(string key)
        {
            return LocExtension.GetLocalizedValue<T>(Assembly.GetCallingAssembly().GetName().Name + ":Strings:" + key);
        }

        #endregion
    }
}
