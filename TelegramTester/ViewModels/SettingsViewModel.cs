﻿//-----------------------------------------------------------------------
// <copyright file="Settings.cs" company="Raphael Pala">
//     Copyright (c) Raphael Pala. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace Pala.TelegramTester
{
    using Caliburn.Micro;
    using MahApps.Metro;
    using MahApps.Metro.Controls.Dialogs;
    using NLog;
    using System;
    using System.Collections.Generic;
    using System.Collections.Specialized;
    using System.ComponentModel;
    using System.Linq;
    using System.Windows.Media;

    /// <summary>
    /// Defines the <see cref="SettingsViewModel" />
    /// </summary>
    internal class SettingsViewModel : Screen, IHandle<Settings>
    {
        #region Fields

        /// <summary>
        /// Defines the 
        /// </summary>
        private static readonly Logger _logger = NLog.LogManager.GetCurrentClassLogger();

        /// <summary>
        /// Defines the _dialogCoordinator
        /// </summary>
        private readonly IDialogCoordinator _dialogCoordinator;

        /// <summary>
        /// Defines the _eventAggregator
        /// </summary>
        private readonly IEventAggregator _eventAggregator;

        /// <summary>
        /// Defines the 
        /// </summary>
        private readonly ISettingsService _settingsService;

        /// <summary>
        /// Defines the _selectedField
        /// </summary>
        private TelegramField _selectedField;

        /// <summary>
        /// Defines the _selectedTelegram
        /// </summary>
        private TelegramDefinition _selectedTelegram;

        #endregion

        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="SettingsViewModel"/> class.
        /// </summary>
        /// <param name="settingsService">The <see cref="ISettingsService"/></param>
        /// <param name="dialogCoordinator"></param>
        /// <param name="eventAggregator"></param>
        public SettingsViewModel(ISettingsService settingsService, IDialogCoordinator dialogCoordinator, IEventAggregator eventAggregator)
        {
            _settingsService = settingsService;
            _dialogCoordinator = dialogCoordinator;
            _eventAggregator = eventAggregator;
            _eventAggregator.Subscribe(this);
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets a value indicating whether ActiveConnection
        /// </summary>
        public bool ActiveConnection
        {
            get => _settingsService.ActiveConnection;
            set
            {
                _settingsService.ActiveConnection = value;
                NotifyOfPropertyChange(() => ActiveConnection);
            }
        }

        /// <summary>
        /// Gets or sets the ApplicationColor
        /// </summary>
        public string ApplicationColor
        {
            get => _settingsService.ApplicationColor;
            set
            {
                _settingsService.ApplicationColor = value;

                NotifyOfPropertyChange(() => ApplicationColor);
            }
        }

        /// <summary>
        /// Gets or sets the ApplicationStyle
        /// </summary>
        public Style ApplicationStyle
        {
            get => _settingsService.ApplicationStyle;
            set
            {
                _settingsService.ApplicationStyle = value;

                NotifyOfPropertyChange(() => ApplicationStyle);
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether AutoSendAck
        /// </summary>
        public bool AutoSendAck
        {
            get => _settingsService.AutosendAck;
            set
            {
                _settingsService.AutosendAck = value;
                NotifyOfPropertyChange(() => AutoSendAck);
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether BigEndianEncoding
        /// </summary>
        public bool BigEndianEncoding
        {
            get => _settingsService.BigEndianEncoding;
            set
            {
                _settingsService.BigEndianEncoding = value;
                NotifyOfPropertyChange(() => BigEndianEncoding);
            }
        }

        /// <summary>
        /// Gets a value indicating whether CanAddField
        /// </summary>
        public bool CanAddField { get => _selectedTelegram != null; }

        /// <summary>
        /// Gets a value indicating whether CanAddRepeatGroup
        /// </summary>
        public bool CanAddRepeatGroup { get => _selectedTelegram != null; }

        /// <summary>
        /// Gets a value indicating whether CanDeleteField
        /// </summary>
        public bool CanDeleteField => _selectedTelegram != null && _selectedField != null;

        /// <summary>
        /// Gets a value indicating whether CanDeleteTelegram
        /// </summary>
        public bool CanDeleteTelegram => _selectedTelegram != null;

        /// <summary>
        /// The CanMoveFieldDown
        /// </summary>
        /// <returns>The <see cref="bool"/></returns>
        public bool CanMoveFieldDown
        {
            get
            {
                //Field can move down if it is not null and field position is not at the end of the field list
                if (SelectedField != null && SelectedTelegram.GetFieldPosition(SelectedField) < TelegramFields.Count - 1)
                {
                    int fieldPosition = SelectedTelegram.GetFieldPosition(SelectedField);

                    //if field is repeat group start and next field is repeat group end, field cannot move (start is not 
                    //allowed to move below end)
                    if (SelectedField.Datatype == Datatype.RepeatGroupStart && SelectedTelegram.FieldList[fieldPosition + 1].Datatype == Datatype.RepeatGroupEnd)
                    {
                        return false;
                    }

                    //if field is repeat group end and next field is repeat group start, field cannot move (end is not 
                    //allowed to move below start)
                    else if (SelectedField.Datatype == Datatype.RepeatGroupEnd && SelectedTelegram.FieldList[fieldPosition + 1].Datatype == Datatype.RepeatGroupStart)
                    {
                        return false;
                    }
                    else
                    {
                        return true;
                    }
                }

                return false;
            }
        }

        /// <summary>
        /// The CanMoveFieldUp
        /// </summary>
        /// <returns>The <see cref="bool"/></returns>
        public bool CanMoveFieldUp
        {
            get
            {
                //Field can move up if it is not null and field position is not at the start of the field list
                if (SelectedField != null && SelectedTelegram.GetFieldPosition(SelectedField) > 0)
                {
                    int fieldPosition = SelectedTelegram.GetFieldPosition(SelectedField);

                    //if field is repeat group start and previous field is repeat group end, field cannot move (start is not 
                    //allowed to move above end)
                    if (SelectedField.Datatype == Datatype.RepeatGroupStart && SelectedTelegram.FieldList[fieldPosition - 1].Datatype == Datatype.RepeatGroupEnd)
                    {
                        return false;
                    }

                    //if field is repeat group end and previous field is repeat group start, field cannot move (end is not 
                    //allowed to move above start)
                    else if (SelectedField.Datatype == Datatype.RepeatGroupEnd && SelectedTelegram.FieldList[fieldPosition - 1].Datatype == Datatype.RepeatGroupStart)
                    {
                        return false;
                    }
                    else
                    {
                        return true;
                    }
                }

                return false;
            }
        }

        /// <summary>
        /// Gets a value indicating whether CanRenameTelegram
        /// </summary>
        public bool CanRenameTelegram => _selectedTelegram != null;

        /// <summary>
        /// Gets a value indicating whether CanSetAckTelegram
        /// </summary>
        public bool CanSetAckTelegram
        {
            get
            {
                if (SelectedTelegram != null)
                {
                    return _settingsService.GetAckTelegram(SelectedTelegram.Direction) == 0
                        || _settingsService.GetAckTelegram(SelectedTelegram.Direction) == SelectedTelegram.TelegramNumber;
                }
                else
                {
                    return false;
                }
            }
        }

        /// <summary>
        /// Gets the Colors
        /// </summary>
        public List<AccentColorData> Colors { get => ThemeManager.Accents.Select(a => new AccentColorData() { Name = a.Name, ColorBrush = a.Resources["AccentColorBrush"] as Brush }).ToList(); }

        /// <summary>
        /// Gets or sets the IPAddressOctet1
        /// </summary>
        public int IPAddressOctet1
        {
            //Split ip address and try to parse first octet
            get => Int32.TryParse(_settingsService.RemoteHost.Split('.')[0], out int octet) ? octet : 10;
            set
            {
                int octet = Clamp(value, 0, 254);
                _settingsService.RemoteHost = octet.ToString() + "." + IPAddressOctet2 + "." + IPAddressOctet3 + "." + IPAddressOctet4;
                NotifyOfPropertyChange(() => IPAddressOctet1);
            }
        }

        /// <summary>
        /// Gets or sets the IPAddressOctet2
        /// </summary>
        public int IPAddressOctet2
        {
            //Split ip address and try to parse second octet
            get => Int32.TryParse(_settingsService.RemoteHost.Split('.')[1], out int octet) ? octet : 1;
            set
            {
                int octet = Clamp(value, 0, 254);
                _settingsService.RemoteHost = IPAddressOctet1 + "." + octet.ToString() + "." + IPAddressOctet3 + "." + IPAddressOctet4;
                NotifyOfPropertyChange(() => IPAddressOctet2);
            }
        }

        /// <summary>
        /// Gets or sets the IPAddressOctet3
        /// </summary>
        public int IPAddressOctet3
        {
            //Split ip address and try to parse third octet
            get => Int32.TryParse(_settingsService.RemoteHost.Split('.')[2], out int octet) ? octet : 1;
            set
            {
                int octet = Clamp(value, 0, 254);
                _settingsService.RemoteHost = IPAddressOctet1 + "." + IPAddressOctet2 + "." + octet.ToString() + "." + IPAddressOctet4;
                NotifyOfPropertyChange(() => IPAddressOctet3);
            }
        }

        /// <summary>
        /// Gets or sets the IPAddressOctet4
        /// </summary>
        public int IPAddressOctet4
        {
            //Split ip address and try to parse fourth octet
            get => Int32.TryParse(_settingsService.RemoteHost.Split('.')[3], out int octet) ? octet : 1;
            set
            {
                int octet = Clamp(value, 0, 254);
                _settingsService.RemoteHost = IPAddressOctet1 + "." + IPAddressOctet2 + "." + IPAddressOctet3 + "." + octet.ToString();
                NotifyOfPropertyChange(() => IPAddressOctet4);
            }
        }

        /// <summary>
        /// Gets or sets the PortNumber
        /// </summary>
        public int PortNumber
        {
            get => _settingsService.Port;
            set
            {
                _settingsService.Port = value;
                NotifyOfPropertyChange(() => PortNumber);
            }
        }

        /// <summary>
        /// Gets or sets the SelectedField
        /// </summary>
        public TelegramField SelectedField
        {
            get => _selectedField;
            set
            {
                _selectedField = value;
                NotifyOfPropertyChange(() => SelectedField);
                NotifyOfPropertyChange(() => CanDeleteField);
                NotifyOfPropertyChange(() => CanMoveFieldUp);
                NotifyOfPropertyChange(() => CanMoveFieldDown);
                NotifyOfPropertyChange(() => ShowFieldValues);
                NotifyOfPropertyChange(() => ValueDataDWord);
                if (_selectedField != null)
                {
                    _logger.Debug("Field selected: {0}, Description: {1}", value, _selectedField.FieldDescription);
                }
            }
        }

        /// <summary>
        /// Gets or sets the SelectedTelegram
        /// </summary>
        public TelegramDefinition SelectedTelegram
        {
            get => _selectedTelegram;
            set
            {
                _selectedTelegram = value;
                NotifyOfPropertyChange(() => SelectedTelegram);
                NotifyOfPropertyChange(() => CanDeleteTelegram);
                NotifyOfPropertyChange(() => CanRenameTelegram);
                NotifyOfPropertyChange(() => CanAddField);
                NotifyOfPropertyChange(() => CanAddRepeatGroup);
                NotifyOfPropertyChange(() => CanDeleteField);
                NotifyOfPropertyChange(() => TelegramFields);
                NotifyOfPropertyChange(() => ShowFieldEdit);
                NotifyOfPropertyChange(() => CanSetAckTelegram);
                if (_selectedTelegram != null)
                {
                    _logger.Debug("Telegram definition selected: {0}, Number: {1}", value, _selectedTelegram.TelegramNumber);
                }
            }
        }

        /// <summary>
        /// Gets or sets the RemoteHost
        /// </summary>
        public String RemoteHost
        {
            get => _settingsService.RemoteHost;
            set
            {
                _logger.Debug("setting remote host address to: {0}", value);
                _settingsService.RemoteHost = value;
                NotifyOfPropertyChange(() => RemoteHost);
            }
        }

        /// <summary>
        /// Gets a value indicating whether ShowFieldEdit
        /// </summary>
        public bool ShowFieldEdit { get => SelectedTelegram != null; }

        /// <summary>
        /// Gets a value indicating whether ShowFieldValues
        /// </summary>
        public bool ShowFieldValues { get => SelectedField != null; }

        /// <summary>
        /// Gets or sets the Simulation
        /// </summary>
        public Simulation Simulation
        {
            get => _settingsService.Simulation;
            set
            {
                _settingsService.Simulation = value;
                _logger.Debug("Setting simulation to: {0}", value);
                NotifyOfPropertyChange(() => Simulation);
            }
        }

        /// <summary>
        /// Gets the TelegramDefinitions
        /// </summary>
        public BindingList<TelegramDefinition> TelegramDefinitions { get => _settingsService.TelegramDefinitions; }

        /// <summary>
        /// Gets the TelegramFields
        /// </summary>
        public BindingList<TelegramField> TelegramFields { get => _selectedTelegram?.FieldList; }

        /// <summary>
        /// Gets or sets the ValueDataDWord
        /// </summary>
        public int ValueDataDWord
        {
            get
            {
                return SelectedField?.ValueDataDWord ?? 0;
            }
            set
            {
                if (SelectedField != null)
                {
                    if (SelectedField.Datatype == Datatype.RepeatGroupStart || SelectedField.Datatype == Datatype.RepeatGroupEnd)
                    {
                        if (value < 1)
                        {
                            value = 1;
                        }
                        else if (value > 100)
                        {
                            value = 100;
                        }
                    }

                    SelectedField.ValueDataDWord = value;
                    NotifyOfPropertyChange(() => SelectedField);
                    NotifyOfPropertyChange(() => ValueDataDWord);
                }
            }
        }

        #endregion

        #region Methods

        /// <summary>
        /// The AddField
        /// </summary>
        public async System.Threading.Tasks.Task AddFieldAsync()
        {
            _logger.Debug("Add field selected.");
            MetroDialogSettings settings = new MetroDialogSettings
            {
                AffirmativeButtonText = LocalizationProvider.GetLocalizedValue<string>("OK"),
                NegativeButtonText = LocalizationProvider.GetLocalizedValue<string>("Cancel")
            };

            string result = await MessageBox.ShowInputAsync(LocalizationProvider.GetLocalizedValue<string>("AddField"),
                LocalizationProvider.GetLocalizedValue<string>("AddFieldMessage"),
                settings).ConfigureAwait(true);

            _logger.Debug("Input field name: {0}", result);
            if (result == null || result.Equals(""))
            {

            }
            else
            {
                _logger.Debug("Trying to add field name: {0}", result);
                if (_selectedTelegram != null)
                {
                    TelegramField setField = _selectedTelegram.AddField(result);
                    SelectedField = setField;
                }
            }
        }

        /// <summary>
        /// The AddRepeatGroup
        /// </summary>
        public void AddRepeatGroup()
        {
            _logger.Debug("Trying to add repeat group");

            if (_selectedTelegram != null)
            {
                TelegramField setField = _selectedTelegram.AddRepeatGroup();
                SelectedField = setField;
            }
        }

        /// <summary>
        /// The AddTelegram
        /// </summary>
        public async System.Threading.Tasks.Task AddTelegramAsync()
        {
            _logger.Debug("Add telegram selected.");
            MetroDialogSettings settings = new MetroDialogSettings
            {
                AffirmativeButtonText = LocalizationProvider.GetLocalizedValue<string>("OK"),
                NegativeButtonText = LocalizationProvider.GetLocalizedValue<string>("Cancel")
            };

            string result = await MessageBox.ShowInputAsync(LocalizationProvider.GetLocalizedValue<string>("AddTelegram"),
                LocalizationProvider.GetLocalizedValue<string>("AddTelegramMessage"),
                settings).ConfigureAwait(true);
            _logger.Debug("Input telegram number: {0}", result);
            if (result == null || result.Equals(""))
            {

            }
            else if (!int.TryParse(result, out int parseInt) || parseInt <= 0 || parseInt > 9999)
            {
                await MessageBox.ShowMessageAsync(LocalizationProvider.GetLocalizedValue<string>("Error"),
                LocalizationProvider.GetLocalizedValue<string>("AddTelegramErrorNotNumber"),
                MessageDialogStyle.Affirmative, settings).ConfigureAwait(true);
            }
            else
            {
                _logger.Debug("Trying to add telegram: {0}", parseInt);
                TelegramDefinition setTelegramDefinition = _settingsService.AddTelegramDefinition(parseInt);
                if (setTelegramDefinition == null)
                {
                    await MessageBox.ShowMessageAsync(LocalizationProvider.GetLocalizedValue<string>("Error"),
                    LocalizationProvider.GetLocalizedValue<string>("AddTelegramErrorAlreadySet"),
                    MessageDialogStyle.Affirmative, settings).ConfigureAwait(true);
                }
                else
                {
                    SelectedTelegram = setTelegramDefinition;
                }
            }
        }

        /// <summary>
        /// The DeleteField
        /// </summary>
        public async System.Threading.Tasks.Task DeleteFieldAsync()
        {
            _logger.Debug("Delete field selected.");

            MetroDialogSettings settings = new MetroDialogSettings
            {
                AffirmativeButtonText = LocalizationProvider.GetLocalizedValue<string>("OK"),
                NegativeButtonText = LocalizationProvider.GetLocalizedValue<string>("Cancel")
            };

            string dialogText;

            //if field is repeat group, modify confirmation dialog
            if (_selectedField.Datatype == Datatype.RepeatGroupStart || _selectedField.Datatype == Datatype.RepeatGroupEnd)
            {
                dialogText = LocalizationProvider.GetLocalizedValue<string>("DeleteRepeatGroup") + "?";
            }
            else
            {
                dialogText = LocalizationProvider.GetLocalizedValue<string>("DeleteFieldMessage") + " " + _selectedField.FieldDescription + "?";
            }

            MessageDialogResult result = await MessageBox.ShowMessageAsync(LocalizationProvider.GetLocalizedValue<string>("DeleteField"), dialogText,
            MessageDialogStyle.AffirmativeAndNegative, settings).ConfigureAwait(true);

            if (result == MessageDialogResult.Affirmative)
            {
                _logger.Debug("Trying to delete field {0}", _selectedField.FieldDescription);
                _selectedTelegram.DeleteField(_selectedField);
            }
        }

        /// <summary>
        /// The DeleteTelegramAsync
        /// </summary>
        /// <returns>The <see cref="System.Threading.Tasks.Task"/></returns>
        public async System.Threading.Tasks.Task DeleteTelegramAsync()
        {
            _logger.Debug("Delete telegram selected.");
            MetroDialogSettings settings = new MetroDialogSettings
            {
                AffirmativeButtonText = LocalizationProvider.GetLocalizedValue<string>("OK"),
                NegativeButtonText = LocalizationProvider.GetLocalizedValue<string>("Cancel")
            };
            MessageDialogResult result = await MessageBox.ShowMessageAsync(LocalizationProvider.GetLocalizedValue<string>("DeleteTelegram"),
                LocalizationProvider.GetLocalizedValue<string>("DeleteTelegramMessage") + " " + _selectedTelegram.TelegramNumber + "?",
                MessageDialogStyle.AffirmativeAndNegative, settings).ConfigureAwait(true);
            if (result == MessageDialogResult.Affirmative)
            {
                _logger.Debug("Trying to delete telegram {0}", _selectedTelegram.TelegramNumber);
                _settingsService.DeleteTelegramDefinition(SelectedTelegram);
            }
        }

        /// <summary>
        /// The Handle
        /// </summary>
        /// <param name="message">The <see cref="Settings"/></param>
        public void Handle(Settings message)
        {
            if (message.Equals(Settings.Load))
            {
                _logger.Debug("Loading settings");
                LoadSettings();
            }
            else if (message.Equals(Settings.Save))
            {
                _logger.Debug("Saving settings");
                SaveSettings();
            }
            else if (message.Equals(Settings.Reload))
            {
                _logger.Debug("Reloading settings");
                ReloadSettings();
            }
        }

        /// <summary>
        /// The LoadSettings
        /// </summary>
        public void LoadSettings()
        {
            _settingsService.LoadSettings();
        }

        /// <summary>
        /// The MoveFieldDown
        /// </summary>
        public void MoveFieldDown()
        {
            TelegramField field = SelectedField;
            _logger.Debug("Trying to move field {0} down.", field.FieldDescription);
            SelectedTelegram.MoveFieldDown(SelectedField);
            SelectedField = field;
        }

        /// <summary>
        /// The MoveFieldUp
        /// </summary>
        public void MoveFieldUp()
        {
            TelegramField field = SelectedField;
            _logger.Debug("Trying to move field {0} up.", field.FieldDescription);
            SelectedTelegram.MoveFieldUp(field);
            SelectedField = field;
        }

        /// <summary>
        /// The ReloadSettings
        /// </summary>
        public void ReloadSettings()
        {
            NotifyOfPropertyChange(() => ApplicationColor);
            NotifyOfPropertyChange(() => ApplicationStyle);
            NotifyOfPropertyChange(() => AutoSendAck);
            NotifyOfPropertyChange(() => ActiveConnection);
            NotifyOfPropertyChange(() => BigEndianEncoding);
            NotifyOfPropertyChange(() => PortNumber);
            NotifyOfPropertyChange(() => Simulation);
            NotifyOfPropertyChange(() => IPAddressOctet1);
            NotifyOfPropertyChange(() => IPAddressOctet2);
            NotifyOfPropertyChange(() => IPAddressOctet3);
            NotifyOfPropertyChange(() => IPAddressOctet4);
            NotifyOfPropertyChange(() => RemoteHost);
        }

        /// <summary>
        /// The RenameTelegramAsync
        /// </summary>
        /// <returns>The <see cref="System.Threading.Tasks.Task"/></returns>
        public async System.Threading.Tasks.Task RenameTelegramAsync()
        {
            _logger.Debug("Rename telegram selected.");
            MetroDialogSettings settings = new MetroDialogSettings
            {
                AffirmativeButtonText = LocalizationProvider.GetLocalizedValue<string>("OK"),
                NegativeButtonText = LocalizationProvider.GetLocalizedValue<string>("Cancel")
            };

            string result = await MessageBox.ShowInputAsync(LocalizationProvider.GetLocalizedValue<string>("RenameTelegram") + " " + SelectedTelegram.TelegramNumber,
                LocalizationProvider.GetLocalizedValue<string>("RenameTelegramMessage"),
                settings).ConfigureAwait(true);
            _logger.Debug("Input new telegram number: {0}", result);
            if (result == null || result.Equals(""))
            {

            }
            else if (!int.TryParse(result, out int parseInt) || parseInt <= 0 || parseInt > 9999)
            {
                await MessageBox.ShowMessageAsync(LocalizationProvider.GetLocalizedValue<string>("Error"),
                LocalizationProvider.GetLocalizedValue<string>("AddTelegramErrorNotNumber"),
                MessageDialogStyle.Affirmative, settings).ConfigureAwait(true);
            }
            else
            {
                _logger.Debug("Trying to rename telegram {0} to {1}", SelectedTelegram.TelegramNumber, parseInt);
                if (_settingsService.FindTelegramDefinition(parseInt) != null)
                {
                    _logger.Debug("Telegram already exists.");
                    await MessageBox.ShowMessageAsync(LocalizationProvider.GetLocalizedValue<string>("Error"),
                    LocalizationProvider.GetLocalizedValue<string>("AddTelegramErrorAlreadySet"),
                    MessageDialogStyle.Affirmative, settings).ConfigureAwait(true);
                }
                else
                {
                    _logger.Debug("Setting new telegram number");
                    SelectedTelegram.TelegramNumber = parseInt;
                    _settingsService.SortTelegramDefinitions();
                }
            }
        }

        /// <summary>
        /// The SaveSettings
        /// </summary>
        public void SaveSettings()
        {
            _settingsService.SaveSettings();
        }

        /// <summary>
        /// The SetBitInBitfield
        /// </summary>
        /// <param name="bit">The <see cref="int"/></param>
        /// <param name="value">The <see cref="bool"/></param>
        public void SetBitInBitfield(int bit, bool value)
        {
            if (SelectedField == null)
            {
                return;
            }

            _logger.Debug("Setting bit {0} to {1}", bit, value);

            BitVector32 bitvector = new BitVector32(SelectedField.ValueDataDWord);
            int bitmask = (int)Math.Pow(2.0, (double)bit);
            bitvector[bitmask] = value;

            SelectedField.ValueDataDWord = bitvector.Data;
            NotifyOfPropertyChange(() => SelectedField);
            NotifyOfPropertyChange(() => ValueDataDWord);
        }

        /// <summary>
        /// The SetLanguage
        /// </summary>
        /// <param name="language">The <see cref="string"/></param>
        public void SetLanguage(string language)
        {
            if (language != null)
            {
                _settingsService.Language = language;
            }
        }

        /// <summary>
        /// The Clamp
        /// </summary>
        /// <param name="value">The <see cref="int"/></param>
        /// <param name="min">The <see cref="int"/></param>
        /// <param name="max">The <see cref="int"/></param>
        /// <returns>The <see cref="int"/></returns>
        private int Clamp(int value, int min, int max)
        {
            return (value < min) ? min : (value > max) ? max : value;
        }

        #endregion
    }
}
