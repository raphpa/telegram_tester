﻿//-----------------------------------------------------------------------
// <copyright file="LoggingViewModel.cs" company="Raphael Pala">
//     Copyright (c) Raphael Pala. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace Pala.TelegramTester
{
    using Caliburn.Micro;
    using NLog;
    using System.Text;
    using System.Windows.Data;

    /// <summary>
    /// Defines the <see cref="Logging" />
    /// </summary>
    public class LoggingViewModel : Screen, IHandle<Telegram>
    {
        #region Fields

        /// <summary>
        /// Defines the _logger
        /// </summary>
        private static readonly Logger _logger = NLog.LogManager.GetCurrentClassLogger();

        /// <summary>
        /// Defines the _eventAggregator
        /// </summary>
        private readonly IEventAggregator _eventAggregator;

        /// <summary>
        /// Defines the _ioService
        /// </summary>
        private readonly IIOService _ioService;

        /// <summary>
        /// Defines the _receiveListLock
        /// </summary>
        private readonly object _receiveListLock = new object();

        /// <summary>
        /// Defines the _sendListLock
        /// </summary>
        private readonly object _sendListLock = new object();

        /// <summary>
        /// Defines the _settingsService
        /// </summary>
        private readonly ISettingsService _settingsService;

        /// <summary>
        /// Defines the _freezeWindow
        /// </summary>
        private bool _freezeWindow = false;

        /// <summary>
        /// Defines the _selectedTelegram
        /// </summary>
        private Telegram _selectedTelegram;

        #endregion

        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="LoggingViewModel"/> class.
        /// </summary>
        /// <param name="eventAggregator"></param>
        /// <param name="ioService"></param>
        /// <param name="settingsService"></param>
        public LoggingViewModel(IEventAggregator eventAggregator, IIOService ioService, ISettingsService settingsService)
        {
            ReceiveTelegramList = new ObservableCollectionEx<Telegram>();
            BindingOperations.EnableCollectionSynchronization(ReceiveTelegramList, _receiveListLock);

            SendTelegramList = new ObservableCollectionEx<Telegram>();
            BindingOperations.EnableCollectionSynchronization(SendTelegramList, _sendListLock);

            _eventAggregator = eventAggregator;
            _eventAggregator.Subscribe(this);

            _ioService = ioService;

            _settingsService = settingsService;
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets a value indicating whether FreezeWindow
        /// </summary>
        public bool FreezeWindow
        {
            get => _freezeWindow;
            set
            {
                _freezeWindow = value;
                ReceiveTelegramList.SupressNotification = value;
                SendTelegramList.SupressNotification = value;
            }
        }

        /// <summary>
        /// Gets or sets the MaximumNumberOfEntries
        /// </summary>
        public int MaximumNumberOfEntries
        {
            get => _settingsService.MaximumLogCount;
            set
            {
                _settingsService.MaximumLogCount = value;
                NotifyOfPropertyChange(() => MaximumNumberOfEntries);
            }
        }

        /// <summary>
        /// Gets the ReceiveTelegramList
        /// </summary>
        public ObservableCollectionEx<Telegram> ReceiveTelegramList { get; }

        /// <summary>
        /// Gets or sets the SelectedTelegram
        /// </summary>
        public Telegram SelectedTelegram
        {
            get => _selectedTelegram;

            set
            {
                _selectedTelegram = null;
                NotifyOfPropertyChange(() => SelectedTelegram);

                _selectedTelegram = value;
                _logger.Debug("Selected telegram: {0}", _selectedTelegram?.TelegramNumber ?? 0);
                NotifyOfPropertyChange(() => SelectedTelegram);

                _selectedTelegram.Direction = Direction.Internal;
                if (_selectedTelegram.RawData == null)
                {
                    _selectedTelegram.RawData = _selectedTelegram.DataAsByteList.ToArray();
                }
                _eventAggregator.PublishOnUIThread(_selectedTelegram);
                _eventAggregator.PublishOnUIThread(Message.ParseInternal);
            }
        }

        /// <summary>
        /// Gets the ReceiveList
        /// </summary>
        public ObservableCollectionEx<Telegram> SendTelegramList { get; }

        #endregion

        #region Methods

        /// <summary>
        /// The ExportToCSV
        /// </summary>
        public void ExportToCSV()
        {
            _ioService.ChooseLogFileSave();
            _logger.Debug("Save logged telegrams to {0}", _ioService.LogFile);

            if (_ioService.LogFile != null)
            {
                StringBuilder stringBuilder = new StringBuilder();

                string header = string.Format("{0},{1},{2},{3}", LocalizationProvider.GetLocalizedValue<string>("Date"), LocalizationProvider.GetLocalizedValue<string>("TelegramNumber"),
                                                                 LocalizationProvider.GetLocalizedValue<string>("TransmissionNumber"), LocalizationProvider.GetLocalizedValue<string>("Data"));

                stringBuilder.AppendLine("//Send");
                stringBuilder.AppendLine(header);

                for (int i = SendTelegramList.Count - 1; i >= 0; i--)
                {
                    Telegram telegram = SendTelegramList[i];

                    string telegramLine = string.Format("{0},{1},{2},{3}", telegram.FormattedTimestamp, telegram.TelegramNumber, telegram.TransmissionNumber, telegram.DataAsString);
                    stringBuilder.AppendLine(telegramLine);
                }

                stringBuilder.AppendLine("//Receive");
                stringBuilder.AppendLine(header);

                for (int i = ReceiveTelegramList.Count - 1; i >= 0; i--)
                {
                    Telegram telegram = ReceiveTelegramList[i];

                    string telegramLine = string.Format("{0},{1},{2},{3}", telegram.FormattedTimestamp, telegram.TelegramNumber, telegram.TransmissionNumber, telegram.DataAsString);
                    stringBuilder.AppendLine(telegramLine);
                }

                _ioService.WriteLog(stringBuilder.ToString());
            }
        }

        /// <summary>
        /// The Handle
        /// </summary>
        /// <param name="message"></param>
        public void Handle(Telegram message)
        {
            //If telegram was received, add to receiving log list
            if (message.Direction == Direction.Receive)
            {
                _logger.Debug("Adding received telegram to receive log");

                //Add telegram to the start of the list
                ReceiveTelegramList.Insert(0, message);

                //As long as list is longer than given number of entries, delete from end of list
                while (ReceiveTelegramList.Count > MaximumNumberOfEntries)
                {
                    ReceiveTelegramList.RemoveAt(ReceiveTelegramList.Count - 1);
                }
            }

            //If telegram was sent, add to sending log list
            else if (message.Direction == Direction.Send)
            {
                _logger.Debug("Adding received telegram to send log");

                //Add telegram to the start of the list
                SendTelegramList.Insert(0, message);

                //As long as list is longer than given number of entries, delete from end of list
                while (SendTelegramList.Count > MaximumNumberOfEntries)
                {
                    SendTelegramList.RemoveAt(SendTelegramList.Count - 1);
                }
            }
        }

        /// <summary>
        /// The ShowParse
        /// </summary>
        public void ShowParse()
        {
            _eventAggregator.PublishOnUIThread(Message.ShowParse);
        }

        #endregion
    }
}
