﻿//-----------------------------------------------------------------------
// <copyright file="TelegramTesterBootstrapper.cs" company="Raphael Pala">
//     Copyright (c) Raphael Pala. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

/// <summary>
/// Bootstrapper for application
/// </summary>

namespace Pala.TelegramTester
{
    using Caliburn.Micro;
    using MahApps.Metro.Controls.Dialogs;
    using System;
    using System.Collections.Generic;
    using System.Windows;

    /// <summary>
    /// This class bootstraps the application
    /// </summary>
    public class TelegramTesterBootstrapper : BootstrapperBase
    {
        #region Fields

        /// <summary>
        /// Defines the 
        /// </summary>
        private SimpleContainer _container;

        #endregion

        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="TelegramTesterBootstrapper" /> class.
        /// </summary>
        public TelegramTesterBootstrapper()
        {
            Initialize();
            LogManager.GetLog = type => new NLogLogger(type);
        }

        #endregion

        #region Methods

        /// <summary>
        /// The BuildUp
        /// </summary>
        /// <param name="instance">The <see cref="object"/></param>
        protected override void BuildUp(object instance)
        {
            _container.BuildUp(instance);
        }

        /// <summary>
        /// The Configure
        /// </summary>
        protected override void Configure()
        {
            _container = new SimpleContainer();
            _container.Singleton<IWindowManager, WindowManager>();
            _container.Singleton<IEventAggregator, EventAggregator>();
            _container.Singleton<ITCPService, TCPService>();
            _container.Singleton<ISettingsService, SettingsService>();
            _container.Singleton<IIOService, IOService>();
            _container.Singleton<IDialogCoordinator, DialogCoordinator>();
            _container.PerRequest<IShell, ShellViewModel>();
            //Views
            _container.Singleton<HomeViewModel>();
            _container.Singleton<SettingsViewModel>();
            _container.Singleton<LoggingViewModel>();
        }

        /// <summary>
        /// The GetAllInstances
        /// </summary>
        /// <param name="service">The <see cref="Type"/></param>
        /// <returns>The <see cref="IEnumerable{object}"/></returns>
        protected override IEnumerable<object> GetAllInstances(Type service)
        {
            return _container.GetAllInstances(service);
        }

        /// <summary>
        /// The GetInstance
        /// </summary>
        /// <param name="service">The <see cref="Type"/></param>
        /// <param name="key">The <see cref="string"/></param>
        /// <returns>The <see cref="object"/></returns>
        protected override object GetInstance(Type service, string key)
        {
            var instance = _container.GetInstance(service, key);
            if (instance != null)
                return instance;
            throw new InvalidOperationException("Could not locate any instances.");
        }

        /// <summary>
        /// Startup of bootstrapper
        /// </summary>
        /// <param name="sender">Sender argument</param>
        /// <param name="e">Startup Event Arguments</param>
        protected override void OnStartup(object sender, StartupEventArgs e)
        {
            DisplayRootViewFor<IShell>();
        }

        #endregion
    }
}
