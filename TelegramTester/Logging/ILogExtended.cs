﻿using Caliburn.Micro;

namespace Pala.TelegramTester
{
    public interface ILogExtended : ILog
    {
        /// <summary>
        /// Logs the message as fatal.
        /// </summary>
        /// <param name="format">A formatted message.</param>
        /// <param name="args">Parameters to be injected into the formatted message.</param>
        void Fatal(string format, params object[] args);

        /// <summary>
        /// Logs the message as debug.
        /// </summary>
        /// <param name="format">A formatted message.</param>
        /// <param name="args">Parameters to be injected into the formatted message.</param>
        void Debug(string format, params object[] args);

        /// <summary>
        /// Logs the message as trace.
        /// </summary>
        /// <param name="format">A formatted message.</param>
        /// <param name="args">Parameters to be injected into the formatted message.</param>
        void Trace(string format, params object[] args);
    }
}
