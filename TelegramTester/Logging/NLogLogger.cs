﻿//-----------------------------------------------------------------------
// <copyright file="NLogLogger.cs" company="Raphael Pala">
//     Copyright (c) Raphael Pala. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

/// <summary>
/// Logging class for caliburn
/// </summary>
namespace Pala.TelegramTester
{
    using System;
    using System.Diagnostics;

    /// <summary>
    /// This class makes caliburn log via nlog
    /// </summary>
    public class NLogLogger : ILogExtended
    {
        #region Fields
        /// <summary>
        /// NLog logger
        /// </summary>
        private readonly NLog.Logger _innerLogger;
        #endregion

        #region Constructors
        /// <summary>
        /// Initializes a new instance of the <see cref="NLogLogger" /> class.
        /// </summary>
        /// <param name="type">type given</param>
        public NLogLogger(Type type)
        {
            if (type == null)
            {
                _innerLogger = NLog.LogManager.GetCurrentClassLogger();
            }
            else
            {
                _innerLogger = NLog.LogManager.GetLogger(type.Name);
            }
        }

        #endregion

        #region ILog Members
        /// <summary>
        /// Logs the message as warn.
        /// </summary>
        /// <param name="format">A formatted message.</param>
        /// <param name="args">Parameters to be injected into the formatted message.</param>
        public void Warn(string format, params object[] args)
        {
            if (_innerLogger.IsWarnEnabled)
            {
                _innerLogger.Warn(format, args);
            }
        }

        /// <summary>
        /// Logs the message as error.
        /// </summary>
        /// <param name="exception">An exception.</param>
        public void Error(Exception exception)
        {
            if (_innerLogger.IsErrorEnabled)
            {
                _innerLogger.Error(exception, exception.Message);
            }

        }

        /// <summary>
        /// Logs the message as fatal.
        /// </summary>
        /// <param name="format">A formatted message.</param>
        /// <param name="args">Parameters to be injected into the formatted message.</param>
        public void Fatal(string format, params object[] args)
        {
            if (_innerLogger.IsFatalEnabled)
            {
                _innerLogger.Debug(format, args);
            }
        }

        /// <summary>
        /// Logs the message as info.
        /// </summary>
        /// <param name="format">A formatted message.</param>
        /// <param name="args">Parameters to be injected into the formatted message.</param>
        public void Info(string format, params object[] args)
        {
            if (_innerLogger.IsInfoEnabled)
            {
                _innerLogger.Info(format, args);
            }
        }

        /// <summary>
        /// Logs the message as debug.
        /// </summary>
        /// <param name="format">A formatted message.</param>
        /// <param name="args">Parameters to be injected into the formatted message.</param>
        public void Debug(string format, params object[] args)
        {
            if (_innerLogger.IsDebugEnabled)
            {
                _innerLogger.Debug(format, args);
            }
        }

        /// <summary>
        /// Logs the message as trace.
        /// </summary>
        /// <param name="format">A formatted message.</param>
        /// <param name="args">Parameters to be injected into the formatted message.</param>
        public void Trace(string format, params object[] args)
        {
            if (_innerLogger.IsTraceEnabled)
            {
                _innerLogger.Trace(format, args);
            }
        }
        #endregion
    }
}
