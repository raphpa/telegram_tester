﻿using System;

namespace Pala.TelegramTester
{
    //
    // Zusammenfassung:
    //     Used to manage logging.
    public static class LogManager
    {
        //
        // Zusammenfassung:
        //     Creates an Caliburn.Micro.ILog for the provided type.
        public static Func<Type, ILogExtended> GetLog;
    }
}